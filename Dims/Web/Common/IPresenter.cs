﻿using System;
using System.Collections.Generic;
using System.Text;
using Dims.DAL;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Specialized;
using System.Web.UI;
using Dims.Web.Controls;

namespace Dims.Web.Common
{
    public interface IPresenter
    {
        void Init();
        void Load();
        void DataBind();

        
        object Select();
        object SelectOne();
        void Delete();
        void BginUpdate();
        void EndUpdate();
        void BeginInsert();
        void EndInsert();
        void CardCancel();

        List<UserMessage> MessagesList {get;}
        bool ShowMessages { get; set; }
        
        object MessageView { get;  }
        void InitMessageView(object msgView);

        bool RebindData { get; set; }


        void SetCurrent(ICollection keys, ICollection values); // , IOrderedDictionary obj
        object GetCurrentItem(bool withData);

        void InitCard(ICard card);

        void InitPager(IPager pager);

        void InitView(IListView view);

        void InitFilterer(IFilter filter);

        void InitSortControl(ISort sort);

        void OnCommand(string p, object p_2);



        void BginInsertInline();

        void EndInsertInline();



        void BeginUpdateInline();
        void EndUpdateInline();
    }



    
}
