﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dims.Web.Common
{
    public interface IReportPresenter
    {
        void InitReportView(IReportView reportView);
        object GetParams();
        bool RebindReportData { get; set; }
    }
}
