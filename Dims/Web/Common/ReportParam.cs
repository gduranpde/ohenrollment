﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;

namespace Dims.Web.Common
{
    public class ReportParam:Attribute
    {
        public string Name;

       

        public ReportParam()
        {
        }

      
        public ReportParam(string name)
        {
            Name = name;
        }
    }

    public class ReportNames : Attribute
    {
        public ReportNames(string reportNames)
        {
            
            ReportNamesString = reportNames;
        }


         public string ReportNamesString;

        public string[] ReportNamesArray
        {
            get
            {
                if (string.IsNullOrEmpty(ReportNamesString))
                    return null;

                return ReportNamesString.Split(',');
            }
        }

        public bool CheckReport(string name)
        {
            StringCollection arr = new StringCollection();
            
            var names = ReportNamesArray;

            if (names == null)
                return true;

            arr.AddRange(names);
            
            return arr.Contains(name);
                
        }
    }
}
