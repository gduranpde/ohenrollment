﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace Dims.Web.Common
{
    public class ControlsFinder
    {
        public static Control Find(Control namingContainer, string controlId)
        {

            if (string.IsNullOrEmpty(controlId))
                return null;

            string [] ids = controlId.Split('$');
            
            Control ret = namingContainer;
            foreach (string id in ids)
            {

                if (string.Equals(id, "Page", StringComparison.OrdinalIgnoreCase))
                {
                    ret = namingContainer.Page;
                }
                else if (string.Equals(id, "Parent", StringComparison.OrdinalIgnoreCase))
                {
                    ret = ret.Parent;
                }
                else
                {
                    ret = ret.FindControl(id);
                }
                if (ret == null)
                    return null;
            }
            return ret;
        }
    }
}
