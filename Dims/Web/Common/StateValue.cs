﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Dims.Web.Common
{
    public struct StateValue<TType>
    {

        public void Init(object level, object obj, object key)
        {
            SessionKey = string.Format("{0}:{1}:{2}",level,obj,key);// string.Format("{0}.{1}", obj.GetType().FullName, prop);
                      
        }
        public void Init( string key)
        {
            SessionKey = key;

        }

        public string SessionKey
        {
            get;
            set;
        }

        public bool HasValue
        {
            get
            {
                return HttpContext.Current.Session[SessionKey] == null;
            }
            
        }


        private TType _Value;
        public TType Value
        {
            get
            {
                if (!HasValue)
                {
                    _Value = (TType)HttpContext.Current.Session[SessionKey];
                }
                return _Value;
            }
            set
            {
                HttpContext.Current.Session[SessionKey] =_Value =  value;
            }
        }


        public static implicit operator TType(StateValue<TType> sv)
        {
            return sv.Value;
        }
        
        
    }
}
