﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dims.Web.Common
{
    public class UserMessage:Exception
    {
        public UserMessage(string text)
            : base(text)
        {
        }
    }
}
