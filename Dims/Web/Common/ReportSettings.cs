﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Dims.Web.Common
{
    public static class ReportSettings
    {
        public static string ReportServer
        {
            get { return ConfigurationManager.AppSettings["ReportServer"]; }
        }
        public static string ReportsFolder
        {
            get { return ConfigurationManager.AppSettings["ReportsFolder"]; }
        }
        public static string ReportService
        {
            get
            {
                return ReportServer + ConfigurationManager.AppSettings["ReportService"];
            }
        }

        public static string ReportExecutionService
        {
            get
            {
                return ReportServer + ConfigurationManager.AppSettings["ReportExecutionService"];
            }
        }

        public static string GetFullName(string shortName)
        {
            return string.Format("{0}{1}", ReportsFolder, shortName);
        }


        /*public static ReportExecutionService CreateReportExecutionService()
        {
            ReportExecutionService rsExec = new ReportExecutionService();
            rsExec.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
            rsExec.Url = ReportExecutionService;

            return rsExec;

        }
        */

    }
}
