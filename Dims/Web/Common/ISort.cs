﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dims.Web.Common
{

    public interface ISort
    {
         event EventHandler Apply;
        void InitData();
        void Fill(object filter);
        void Init(object filter);
    }

    /*public interface ISort<TFilterEntity>:ISort
    {
       
    }*/
}
