﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dims.Web.Controls
{
    public interface IAlert<TEntity>
    {
        void Show(TEntity entity);
        void InitPresenter();
    }

    abstract public class Alert<TPresenter , TEntity> : ControlWithPresenter<TPresenter>, IAlert<TEntity>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Visible = false;
            }

            InitPresenter();
        }

        public void Ok()
        {
            Visible = false;
        }

        public void Cancel()
        {
            Visible = false;
        }

        public abstract void Show(TEntity entity);

        public abstract void InitPresenter();

        
    }
}
