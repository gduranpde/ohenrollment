﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Reflection;
using Dims.Web.Common;

namespace Dims.Web.Controls
{
    [SupportsEventValidation]
    [ParseChildren(true)]
    [PersistChildren(true)]
    [Themeable(true)]
    public class UpdatePanelEx : UpdatePanel
    {
        private ITemplate _ProgressTemplate;
        [TemplateContainer(typeof(ContentHolder)),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateInstance(TemplateInstance.Single),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ITemplate ProgressTemplate
        {
            get { return _ProgressTemplate; }
            set { _ProgressTemplate = value; }
        }

        protected ContentHolder ProgressTemplateContainer = new ContentHolder();
            

        public override Control FindControl(string id)
        {
            return
              ProgressTemplateContainer.FindControl(id) ??
              base.FindControl(id);
        }

        public string TriggerControlsIDs { get; set; }


        protected virtual void OnCreateContents()
        {

        }

        private void CreateContents()
        { 
            
            if (ProgressTemplate != null)
            {

                OnCreateContents();

              


            }

			if (!string.IsNullOrEmpty(TriggerControlsIDs))
			{
				string []ids = TriggerControlsIDs.Split(',');
				foreach (string id in ids)
				{
					this.Triggers.Add(new ExtendedTrigger { ControlID = id });
				}
            }
            
            
            
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Page.InitComplete += new EventHandler(Page_InitComplete);
            
        }

        void Page_InitComplete(object sender, EventArgs e)
        {
            CreateContents();
        }

        private ProgressiveUpdatePanelStyle _ProgressStyle;

        public ProgressiveUpdatePanelStyle ProgressStyle
        {
            get
            {
                if (_ProgressStyle == null)
                {
                    _ProgressStyle = new ProgressiveUpdatePanelStyle
                    {
                        InitialDelayTime = 1000,
                        Transparency = 1,
                        CssClass = ""
                    };


                }
                return _ProgressStyle;

            }
        }
        
    }

    public class ProgressiveUpdatePanelStyle 
    {
        public int InitialDelayTime { get; set; }
        public int Transparency{ get; set; }
        public string CssClass { get; set; }

    }

    public class ExtendedTrigger : AsyncPostBackTrigger
    {

        protected Control TrigerControl;

        private Page _page;
        private ScriptManager _scriptManager;


        protected ScriptManager ScriptManager
        {
            get
            {
                if (this._scriptManager == null)
                {
                   
                    this._scriptManager = ScriptManager.GetCurrent(_page);
                
                }
                return this._scriptManager;
            }
        }

        

        protected override void Initialize()
        {
            Init();
            
                        
            
        }

      

       
        protected void Init()
        {
            TrigerControl = ControlsFinder.Find(Owner.NamingContainer, ControlID);


            Owner.Page.Trace.Write(Owner.UniqueID, (TrigerControl != null ? TrigerControl.UniqueID : "none(" + ControlID + ")"));

            if (TrigerControl == null)
                return;

            _page = Owner.Page;

            ScriptManager.RegisterAsyncPostBackControl(this.TrigerControl);
            string eventName = this.EventName;
            if (eventName.Length != 0)
            {
                EventInfo info = this.TrigerControl.GetType().GetEvent(eventName, BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                MethodInfo method = info.EventHandlerType.GetMethod("Invoke");
                ParameterInfo[] parameters = method.GetParameters();
                Delegate handler = Delegate.CreateDelegate(info.EventHandlerType, this, method);
                info.AddEventHandler(this.TrigerControl, handler);
            }
        }

    
        
    }
}
