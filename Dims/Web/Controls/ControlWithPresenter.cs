﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using Dims.Web.Common;

namespace Dims.Web.Controls
{
    public class ControlWithPresenter<TPresenter>: UserControl 
    {


        private string _PresenterID;


        [IDReferenceProperty(typeof(IPresenter))]
        public string PresenterID
        {
            get { return _PresenterID; }
            set { _PresenterID = value; }
        }

        private TPresenter _Presenter;
        protected virtual TPresenter Presenter
        {
            get
            {
                if (_Presenter == null)
                {
                    _Presenter = (TPresenter)(object)FindControl(PresenterID);

                }
                return _Presenter;
            }

        }



        public override Control FindControl(string id)
        {
            if (string.IsNullOrEmpty(id))
                return null;
            Control cur = NamingContainer;


            while (cur != null)
            {
                Control ret = cur.FindControl(id);
                if (ret != null)
                {
                    return ret;
                }

                cur = cur.NamingContainer;
            }

            return base.FindControl(id);
        }



    }

    public class ControlWithPresenter : ControlWithPresenter<IPresenter> 
    {

    }
}
