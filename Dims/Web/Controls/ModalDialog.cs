﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.HtmlControls;

using System.ComponentModel;

namespace Dims.Web.Controls
{
    [SupportsEventValidation]
    [ParseChildren(true)]
    [PersistChildren(true)]
    public class ModalDialog:WebControl
    {
        public ModalDialog()
        {
           
          
        }

                    

        protected override void OnLoad(EventArgs e)
        {
            string script = 
                "<script>function closeModalDialog(dialogId , hiddenId)" +
                "{" +
                "    var dialog = window.document.getElementById(dialogId);" +
                "    var hid = window.document.getElementById(hiddenId); " +
                "    dialog.style.display = 'none';" +
                "    hid.value = 'cancel';" +
                "    return false;" +
                "}</script>";
            Page.RegisterClientScriptBlock("ModalDialogCloseScript", script);

            Visible = true;
            if (string.Compare(StateHidden.Value, "cancel", true) == 0)
            {
                CallMethod(MethodCancel);
                StateHidden.Value = "";

            }

           

            base.OnLoad(e);
        }

       
               




        private Control _DialogControl;
        public Control DialogControl
        {
            get
            {
                if (_DialogControl == null)
                    _DialogControl = FindControl(ControlID);
                return _DialogControl;
            }
        }


        public WebControl GetOkButton()
        {

            return this.FindControl(BtnOkID) as WebControl;
            
        }

        public WebControl GetCancelButton()
        {

            return this.FindControl(BtnCancelID) as WebControl;

        }

        



        protected override void OnPreRender(EventArgs e)
        {
          

            try
            {
                Visible = DialogControl.Visible;
                
            }
            catch 
            {
                Visible = false;
            }
            Control BtOk = GetOkButton(); //this.FindControl(BtnOkID);
            if (BtOk is IButtonControl)
            {
                BtOk.Visible = ShowOkButton;
            }
            WebControl BtCancel = GetCancelButton(); 
            if (BtCancel is IButtonControl)
            {
                BtCancel.Visible = ShowCancelButton;
                BtCancel.Attributes["onclick"] = string.Format("return closeModalDialog('{0}' , '{1}')", this.ClientID, StateHidden.ClientID);
            }

            WebControl BtClose = (WebControl)this.FindControl(BtnCloseID);
            if (BtClose is IButtonControl)
            {
                BtClose.Attributes["onclick"] = string.Format("return closeModalDialog('{0}' , '{1}')", this.ClientID, StateHidden.ClientID);
            }
            
            base.OnPreRender(e);
        }

        //public override ControlCollection  Controls
        //{
        //    get
        //    {
        //        return Main.Controls;
        //    }
        //}
        //public override Control FindControl(string id)
        //{
        //    return Main.FindControl(id);
        //}

        private string _ControlID;

        public string ControlID
        {
            get { return _ControlID; }
            set { _ControlID = value; }
        }

        private string _MethodOk;

        public string MethodOk
        {
            get { return _MethodOk; }
            set { _MethodOk = value; }
        }

        private string _MethodCancel;

        public string MethodCancel
        {
            get { return _MethodCancel; }
            set { _MethodCancel = value; }
        }

        private string _BtnOk;

        public string BtnOkID
        {
            get { return _BtnOk; }
            set { _BtnOk = value; }
        }

        private string _BtnCancel;

        public string BtnCancelID
        {
            get { return _BtnCancel; }
            set { _BtnCancel = value; }
        }

        private string _BtnCloseID;

        public string BtnCloseID
        {
            get { return _BtnCloseID; }
            set { _BtnCloseID = value; }
        }


        private bool _ShowOkButton = true;

        public bool ShowOkButton
        {
            get { return _ShowOkButton; }
            set { _ShowOkButton = value; }
        }


        private bool _ShowCancelButton = true;

        public bool ShowCancelButton
        {
            get { return _ShowCancelButton; }
            set { _ShowCancelButton = value; }
        }


      /*  protected Table Main = new Table();        
       
        protected TableRow HeaderRow = new TableRow();
        protected TableRow ContentRow = new TableRow();
        protected TableRow FooterRow = new TableRow();
*/
        protected Control HeaderCell = new Control();
        protected Control ContentCell = new Control();
        protected Control FooterCell = new Control();
        protected Control WindCell = new Control();

       


        private ITemplate _templHeader;
        private ITemplate _templContent;
        private ITemplate _templFooter;
        private ITemplate _templWindow;



        [TemplateContainer(typeof(ContentHolder)),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateInstance(TemplateInstance.Single),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ITemplate Window
        {
            get { return _templWindow; }
            set { _templWindow = value; }
        }

        [TemplateContainer(typeof(ContentHolder)),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateInstance(TemplateInstance.Single),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ITemplate Header
        {
            get { return _templHeader; }
            set { _templHeader = value; }
        }


        [TemplateContainer(typeof(ContentHolder)),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateInstance(TemplateInstance.Single),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ITemplate Content
        {
            get { return _templContent; }
            set { _templContent = value; }
        }
        [TemplateContainer(typeof(ContentHolder)),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateInstance(TemplateInstance.Single),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ITemplate Footer
        {
            get { return _templFooter; }
            set { _templFooter = value; }
        }
        //public override Control FindControl(string id)
        //{
        //    EnsureChildControls();
        //    return _ctlContent.FindControl(id);
        //}


        ContentHolder HeaderContainer = new ContentHolder();
        ContentHolder FooterContainer = new ContentHolder();
        ContentHolder ContentContainer = new ContentHolder();
        ContentHolder WindowContainer = new ContentHolder();


        public override Control FindControl(string id)
        {
            return
                
                HeaderContainer.FindControl(id)
            ??  ContentContainer.FindControl(id)
            ??  FooterContainer.FindControl(id)
            ??  WindowContainer.FindControl(id)
            ??  base.FindControl(id);
        }

        public string HeaderID { get; set; }
        public string FooterID { get; set; }
        public string ContentID { get; set; }

        private void CreateContents()
        {
            if (Window == null)
                return;
            
            Window.InstantiateIn(WindowContainer);
            this.Controls.Add(WindowContainer);


            if (Header != null && !string.IsNullOrEmpty(HeaderID))
            {
                var head = WindowContainer.FindControl(HeaderID);
                if (head != null)
                {
                    Header.InstantiateIn(HeaderContainer);
                    head.Controls.Add(HeaderContainer);
                }

            }

            if (Content != null && !string.IsNullOrEmpty(ContentID))
            {
                var cont = WindowContainer.FindControl(ContentID);
                if (cont != null)
                {
                    Content.InstantiateIn(ContentContainer);

                    cont.Controls.Add(ContentContainer);
                }
                
            }
            
           
            if (Footer != null && !string.IsNullOrEmpty(FooterID))
            {   var foot = WindowContainer.FindControl(FooterID);
                if (foot != null)
                {
                    Footer.InstantiateIn(FooterContainer);

                    foot.Controls.Add(FooterContainer);

                }
            }
            

            Control BtOk = this.FindControl(BtnOkID);
            if (BtOk is IButtonControl)
            {
                ((IButtonControl)BtOk).Click += new EventHandler(OkButton_Click);
                BtOk.Visible = ShowOkButton;
            }
            
            WebControl BtCancel = (WebControl)this.FindControl(BtnCancelID);

            if (BtCancel is IButtonControl)
            {
                ((IButtonControl)BtCancel).Click += new EventHandler(CancelButton_Click);
                BtCancel.Visible = ShowCancelButton;
               
            }


            this.Controls.Add(StateHidden);


          
        }
        protected HiddenField StateHidden = new HiddenField();


        public void CallMethod(string name)
        {

            if (name == MethodOk && OnOk != null)
                OnOk(this, null);

            if (name == MethodCancel && OnCancel != null)
                OnCancel(this, null);


            if (DialogControl == null)
                throw new Exception("cant find dialog control " + ControlID);



            Type type = DialogControl.GetType();

            try
            {
                type.GetMethod(name).Invoke(DialogControl, null);
            }
            catch(Exception exp)
            {
                if (ThrowExceptions)
                    throw exp.InnerException;
            }
        }

        void CancelButton_Click(object sender, EventArgs e)
        {

         

            CallMethod(MethodCancel);
          

        }

        

        void OkButton_Click(object sender, EventArgs e)
        {
            


              CallMethod(MethodOk);
        }



        protected override void OnInit(EventArgs e)
        {
            CreateContents();
            base.OnInit(e);

           
            
        }


        public bool ThrowExceptions { get; set; }

        public event EventHandler OnOk;
        public event EventHandler OnCancel;
    }

    public class ContentHolder : PlaceHolder, INamingContainer
    {
    }
}
