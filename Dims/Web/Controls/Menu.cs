using System;
using System.Collections.Generic;
using System.Text;

using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;
using System.Collections.Specialized;

namespace Dims.Web.Controls
{

    public class Menu : WebControl
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
           // Page.ClientScript.RegisterClientScriptInclude("AspxMenu", "/Common/js/Menu.js");

            Page.ClientScript.RegisterClientScriptBlock
            (   typeof(string)
                ,"AspxMenu",
                
                "function Menu_SelectTab(MenuID,TabsCount,TabNum)"+
                "{"+
                "   for (i=0; i<TabsCount; i++)"+
                "   {"+
                "       var tab =  document.getElementById(MenuID + '$MenuTab$' + i  );"+
                "       var con =  document.getElementById(MenuID + '$MenuContainer$' + i  );"+
                "       tab.className = 'MenuItemTab';"+
                "       con.className = 'MenuContainer';"+
                "   }"+
                "   var sTab =   document.getElementById(MenuID + '$MenuTab$' + TabNum  );"+
                "   var sCon  =   document.getElementById(MenuID + '$MenuContainer$' + TabNum  );"+
                "   sTab.className = 'MenuItemTabSelected';"+
                "   sCon.className = 'MenuContainerSelected';"+
                "   event.returnValue = false;"+
                "   }"
                ,true
            );
            
        }
        public override string CssClass
        {
            get
            {

                if (string.IsNullOrEmpty(base.CssClass))
                    return "MainMenu";

                return base.CssClass;
            }

        }

        protected string BorderWidthValue
        {
            get
            {
                return BorderWidth.Value.ToString();
            }
        }

        public int HeightCount = 3;

        private void WriteTable(HtmlTextWriter w, string Class, string border, string cellpad, string cellspace)
        {
            if (!string.IsNullOrEmpty(Class))
                w.AddAttribute(HtmlTextWriterAttribute.Class, Class);
            if (!string.IsNullOrEmpty(border))
                w.AddAttribute(HtmlTextWriterAttribute.Border, border);
            if (!string.IsNullOrEmpty(cellpad))
                w.AddAttribute(HtmlTextWriterAttribute.Cellpadding, cellpad);
            if (!string.IsNullOrEmpty(cellspace))
                w.AddAttribute(HtmlTextWriterAttribute.Cellspacing, cellspace);
            w.RenderBeginTag(HtmlTextWriterTag.Table);
        }
        //protected string CurrentUrl
        //{
        //    get
        //    {

        //        return Page.Request.Path;
        //    }
        //}
        protected bool HasActive(SiteMapNode node)
        {
            if (IsActive(node))
                return true;
            
            return Current.IsDescendantOf(node);
        }

        private SiteMapNode _Current;

        public SiteMapNode Current
        {
            get 
            {
                if (_Current == null)
                {
                    _Current = SiteMap.CurrentNode;
                }
                return _Current; 
            }
            set { _Current = value; }
        }


        protected bool IsActive(SiteMapNode node)
        {
            if (node == null || Current == null)
                return false;
            return Current.Equals(node);

        }
        public string MenuTabID(int n)
        {
            return this.ClientID + "$MenuTab$" + n;
        }
        public string MenuContainerID(int n)
        {
            return this.ClientID + "$MenuContainer$" + n;
        }

        public string GetNodeClass(SiteMapNode node)
        {
            StringDictionary s = GetNodeSettings(node);
            return s["class"];
        }

        public StringDictionary GetNodeSettings(SiteMapNode node)
        {
            
            StringDictionary  ret = new StringDictionary();
           
            if (!string.IsNullOrEmpty(node.Description))
            { 
                try
                {
                    string[] col = node.Description.Split(";".ToCharArray());
                    if (col != null)
                    foreach (string c in col)
                    {
                        string []kv = c.Split(":".ToCharArray());
                        if (kv!=null && kv.Length>=2)
                        {
                            ret.Add(kv[0],kv[1]);
                        }


                    }
                }
                catch
                {

                }

            }

            return ret;
        }

        protected void RenderMenu(HtmlTextWriter w, SiteMapNode root)
        {
            if (!Visible)
                return;




            int TabNum = 0;

            WriteTable(w, CssClass, "0", "0", "0");
            w.RenderBeginTag(HtmlTextWriterTag.Tr);
            w.RenderBeginTag(HtmlTextWriterTag.Td);

            WriteTable(w, "Menu", "0", "0", "0");
            w.RenderBeginTag(HtmlTextWriterTag.Tr);

            int RealTabsCount = root.ChildNodes.Count;
            int TabsCount = RealTabsCount + 2;

            w.AddAttribute(HtmlTextWriterAttribute.Class, "MenuItemTabFirst");
            w.RenderBeginTag(HtmlTextWriterTag.Td);
            w.Write("&nbsp;");
            w.RenderEndTag();


            foreach (SiteMapNode node in root.ChildNodes)
            {
              
                w.AddAttribute(HtmlTextWriterAttribute.Class, (HasActive(node)) ? "MenuItemTabSelected" : "MenuItemTab");
                w.AddAttribute(HtmlTextWriterAttribute.Id, MenuTabID(TabNum));
                w.RenderBeginTag(HtmlTextWriterTag.Td);


                w.AddAttribute(HtmlTextWriterAttribute.Onclick, "Menu_SelectTab('" + ClientID + "'," + RealTabsCount + "," + TabNum + ")");



                w.AddAttribute(HtmlTextWriterAttribute.Class, "MenuItem");
                w.AddAttribute(HtmlTextWriterAttribute.Href, "#");
                w.RenderBeginTag(HtmlTextWriterTag.A);
                w.Write(node.Title);
                w.RenderEndTag();
                w.RenderEndTag();

                TabNum++;
            }

            w.AddAttribute(HtmlTextWriterAttribute.Class, "MenuItemTabLast");
            w.RenderBeginTag(HtmlTextWriterTag.Td);
            w.Write("&nbsp;");
            w.RenderEndTag();

            w.RenderEndTag();

            w.RenderBeginTag(HtmlTextWriterTag.Tr);

            w.AddAttribute(HtmlTextWriterAttribute.Class, "MenuItemTabBody");
            w.AddAttribute(HtmlTextWriterAttribute.Colspan, TabsCount.ToString());
            w.RenderBeginTag(HtmlTextWriterTag.Td);


            TabNum = 0;
            foreach (SiteMapNode node in root.ChildNodes)
            {
                w.AddAttribute(HtmlTextWriterAttribute.Id, MenuContainerID(TabNum));
                WriteTable(w, (HasActive(node)) ? "MenuContainerSelected" : "MenuContainer", "0", "0", "3");

                w.RenderBeginTag(HtmlTextWriterTag.Tr);


                foreach (SiteMapNode node2 in node.ChildNodes)
                {


                    w.RenderBeginTag(HtmlTextWriterTag.Td);



                    WriteTable(w, "SubMenuContainer", "0", "0", "0");

                    int Columns = (int)Math.Ceiling(((decimal)node2.ChildNodes.Count) / HeightCount);

                    for (int i = 0; i < HeightCount; i++)
                    {
                        w.RenderBeginTag(HtmlTextWriterTag.Tr);
                        for (int j = 0, c = i; j < Columns; j++, c += HeightCount)
                        {
                            w.RenderBeginTag(HtmlTextWriterTag.Td);
                            if (c < node2.ChildNodes.Count)
                            {
                                SiteMapNode node3 = node2.ChildNodes[c];

                                w.AddAttribute(HtmlTextWriterAttribute.Class, (HasActive(node3)) ? "SubMenuItemSelected" : "SubMenuItem");
                                w.AddAttribute(HtmlTextWriterAttribute.Href, Page.ResolveUrl( node3.Url));
                                w.RenderBeginTag(HtmlTextWriterTag.A);

                                string cl = GetNodeClass(node3);
                                if (!string.IsNullOrEmpty(cl))
                                    w.AddAttribute(HtmlTextWriterAttribute.Class,cl);
                                  
                                w.RenderBeginTag(HtmlTextWriterTag.Span);
                              
                                w.Write(node3.Title); 
                                
                                w.RenderEndTag();
                                
                                w.RenderEndTag();
                            }
                            else
                            {
                                w.Write("&nbsp;");
                            }
                            w.RenderEndTag();

                        }
                        w.RenderEndTag();

                    }

                    w.RenderBeginTag(HtmlTextWriterTag.Tr);
                    w.AddAttribute(HtmlTextWriterAttribute.Colspan, Columns.ToString());
                    w.AddAttribute(HtmlTextWriterAttribute.Class, "Footer");
                    w.RenderBeginTag(HtmlTextWriterTag.Td);

                    w.Write(node2.Title);
                    w.RenderEndTag();
                    w.RenderEndTag();


                    w.RenderEndTag();
                    w.RenderEndTag();
                }

                w.RenderEndTag();
                w.RenderEndTag();
                TabNum++;
            }


            w.RenderEndTag();
            w.RenderEndTag();
            w.RenderEndTag();

            w.RenderEndTag(); 
            w.RenderEndTag();
            w.RenderEndTag();
        }
        protected override void Render(HtmlTextWriter w)
        {

            if (DesignMode)
            {

                w.Write("[Tabbed Menu]");


            }
            else
                RenderMenu(w, SiteMap.RootNode);




        }

    }
}
