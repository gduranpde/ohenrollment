﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using Dims.Web.Common;
using Dims.DAL;
using System.Collections;
using System.Web;
using System.Configuration;

namespace Dims.Web.Controls
{
    public abstract class Presenter<TEntity, TFilterEntity, TView, TListView, TFilter, TCard> :
        Control,
        IPresenter

        where TView : IView
        where TListView : IListView
        where TCard : ICard<TEntity>
        where TFilter : IFilter<TFilterEntity>
    {
        public TListView ListView { get; protected set; }
        public TFilter Filterer { get; protected set; }

        public TCard Card { get; protected set; }


        public IPager Pager { get; protected set; }

        public ISort Sorter { get; protected set; }
        public void InitSortControl(ISort sort)
        {
            Sorter = sort;
            Sorter.Apply+=new EventHandler(Sorter_Apply);
        }

        void Sorter_Apply(object sender, EventArgs e)
        {

            FilterApplayed = true;
            OnSortApply();
        }

        protected virtual void OnSortApply()
        {

        }


      

        public TView View
        {
            get { return (TView)(IView)this.Page; }
        }

        public virtual ITabler<TEntity> Tabler
        {
            get
            {
                throw new Exception("Prsenter must implement this property");
            }

        }

        private ITable<TEntity> _Table;
        public ITable<TEntity> Table
        {
            get
            {
                if (_Table == null)
                {

                    _Table = Tabler.GetTable();
                }
                return _Table;
            }


        }

        protected bool _RebindData = false;
        public bool RebindData
        {
            get { return _RebindData; }
            set { _RebindData = value; }
        }

        public Presenter()
        {






        }

        public bool ShowMessages { get; set; }


        private List<UserMessage> _MessagesList = new List<UserMessage>();
        public List<UserMessage> MessagesList
        {
            get
            {
                return _MessagesList;
            }

        }


        public bool HasErrors
        {
            get
            {
                try
                {
                    return MessagesList.Count > 0;
                }
                catch { }
                return false;
            }
        }
        public void ShowUserMessage(Exception exp)
        {
            if (ShowMessages)
            {
                Exception tmp = exp;
                while (exp != null)
                {

                    if (exp is UserMessage)
                    {
                        MessagesList.Add((UserMessage)exp);
                    }
                    exp = exp.InnerException;
                }

            }
            else
            {
                throw exp;
            }
        }

        public object MessageView { get; protected set; }
        public void InitMessageView(object msgView)
        {
            if (msgView != null)
            {
                ShowMessages = true;
            }

            MessageView = msgView;
        }

        public virtual void OnCommand(string commandName, object commandArgument)
        {

        }



        protected StateValue<TFilterEntity> _Filter;

        public TFilterEntity Filter
        {
            get
            {
                if (_Filter.Value == null)
                {

                    _Filter.Value = Activator.CreateInstance<TFilterEntity>();

                    InitFilter();

                }
                return _Filter.Value;
            }
            set { _Filter.Value = value; }
        }

        protected void InitFilter()
        {
            Page.Trace.Write("presenter", "InitFilter");
            FilterApplayed = true;
            OnInitFilter();
            Page.Trace.Write("presenter", "InitFilter /");
        }

        protected virtual void OnInitFilter()
        {

        }

        protected StateValue<TEntity> _Current;

        public virtual TEntity Current
        {
            get
            {
                return _Current;
            }
            set
            {
                _Current.Value = value;
            }
        }

        public virtual void SetCurrent(TEntity obj)
        {
            Current = obj;
        }

        public virtual TEntity GetCurrent()
        {

            return Table.GetOne(Current);
        }
        public virtual TEntity GetCurrent(TEntity obj)
        {
            SetCurrent(obj);

            return Table.GetOne(Current);
        }
        public virtual void SetCurrent(ICollection keys, ICollection values) // IOrderedDictionary obj
        {
            Current = (TEntity)Reflector.EntityFromParams(typeof(TEntity), keys, values);
        }

        public virtual TEntity GetOneByPK(object obj)
        {
            return Table.GetOne(obj);
        }


        /* private List<TEntity> _SelectedItems;
         public List<TEntity> SelectedItems
         {
             get
             {
                 if (_SelectedItems == null)
                 {
                     _SelectedItems = new List<TEntity>();

                     foreach (IOrderedDictionary key in ListView.SelectedItemsKeys)
                     {

                         TEntity sel = (TEntity)Reflector.EntityFromParams(typeof(TEntity), key  );
                         TEntity cur = Table.GetOne(sel);
                         _SelectedItems.Add(cur);
                     }
                 }

                 return _SelectedItems;
             }
         }
         */



        public virtual new void DataBind()
        {
            Page.Trace.Write("presenter", "DataBind");
            if (FilterApplayed)
            {
                if (Filterer != null)
                    Filterer.Fill(Filter);

                if (Sorter != null)
                    Sorter.Fill(Filter);
                    
                if (Pager != null)
                    Pager.PageIndex = 0;

                RebindData = true;
            }

            if (RebindData && ListView != null)
            {
                OnBeforeDataBind();
                ListView.DataBind();


            }
            OnDataBind();
            Page.Trace.Write("presenter", "DataBind /");
        }

        public virtual void OnBeforeDataBind()
        {
        }

        public virtual void InitCard(ICard card)
        {
            Card = (TCard)card;

            Card.OnOk += new EventHandler(Card_OnOk);
            Card.OnCancel += new EventHandler(card_OnCancel);
            Card.ViewMode_Changed += new EventHandler(Card_ViewMode_Changed);
            
        }

        protected void Card_ViewMode_Changed(object sender, EventArgs e)
        {
            OnCardViewModeChanged();
        }

        protected virtual void OnCardViewModeChanged()
        {
          
        }

        public void InitFilterer(IFilter filter)
        {
            Filterer = (TFilter)filter;
            Filterer.Apply += new EventHandler(Filterer_Apply);

        }

        protected bool FilterApplayed = false;

        void Filterer_Apply(object sender, EventArgs e)
        {

            FilterApplayed = true;
            OnFilterApply();
        }

        protected virtual void OnFilterApply()
        {

        }
        public void InitView(IListView view)
        {
            ListView = (TListView)view;

        }
        public void InitPager(IPager pager)
        {
            Pager = pager;


        }

        void card_OnCancel(object sender, EventArgs e)
        {
            CardCancel();
        }

        void Card_OnOk(object sender, EventArgs e)
        {

            CardOk();

        }


        public virtual void CardOk()
        {
            switch (Card.ViewMode)
            {
                case ViewMode.Add:
                    EndInsert();
                    break;

                case ViewMode.Edit:
                    EndUpdate();
                    break;
            }
        }
        public virtual void CardCancel()
        {
            Card.ViewMode = ViewMode.View;
        }


        /*   void Filter_Apply(object sender, EventArgs e)
           {
            
               Filterer.Fill(Filter);
               Pager.PageIndex = 0;
               RebindData = true;
           }*/

        public virtual void InitSessionStateData()
        {
            _Current.Init(Page, this.ID, "Current");
            _Filter.Init(Page, this.ID, "Filter");
        }

        protected override void OnInit(EventArgs e)
        {

            InitSessionStateData();
            // Page.Trace.Write("Site Info", string.Format("DbServer:{0}, DbName:{1}, ReportServer:{2}, ReportFolder:{3}", Table.db.Connection.DataSource, Table.db.Connection.Database, ReportSettings.ReportServer, ReportSettings.ReportsFolder));

            base.OnInit(e);
            Page.InitComplete += new EventHandler(Page_InitComplete);
        }

        void Page_InitComplete(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
                Init();
        }

        protected override void OnLoad(EventArgs e)
        {
            Page.PreRender += new EventHandler(Page_PreRender);




            Load();
            base.OnLoad(e);
        }

        void Page_PreRender(object sender, EventArgs e)
        {
            Page.Trace.Write("presenter", "Page_PreRender");
            DataBind();
            Page.Trace.Write("presenter", "Page_PreRender /");
        }

        public new void Init()
        {
            Page.Trace.Write("presenter", "Init");

            FilterApplayed = true;

            if (Card != null)
                Card.ViewMode = ViewMode.View;

            OnBeforeLoadFilter();

            if (Filterer != null)
            {
                
                Filterer.InitData();
                Filterer.Init(Filter);
            }

            if (Sorter != null)
            {
                Sorter.InitData();
                Sorter.Init(Filter);
            }

            this.OnInit();

            Page.Trace.Write("presenter", "Init /");

        }

        protected virtual  void OnBeforeLoadFilter()
        {
           
        }

        public new void Load()
        {
            Page.Trace.Write("presenter", "Load");
            OnLoad();
            Page.Trace.Write("presenter", "Load /");
        }

        public new void Delete()
        {
            Page.Trace.Write("presenter", "Delete");
            OnDelete();
            RebindData = true;
            Page.Trace.Write("presenter", "Delete /");
        }
        public void BginUpdate()
        {
            Page.Trace.Write("presenter", "BginUpdate");
            if (Card != null)
            {
                Card.ViewMode = ViewMode.Edit;
                OnBeginUpdate();
            }
            Page.Trace.Write("presenter", "BginUpdate /");
        }
        public void EndUpdate()
        {
            Page.Trace.Write("presenter", "EndUpdate");
            try
            {
                if (Card != null)
                {
                    OnEndUpdate();
                    Card.ViewMode = ViewMode.View;
                    RebindData = true;
                }

                Current = default(TEntity);
            }
            catch (UserMessage exp)
            {
                ShowUserMessage(exp);
            }
            Page.Trace.Write("presenter", "EndUpdate /");
        }

        public void BeginInsert()
        {
            Page.Trace.Write("presenter", "BginInsert");
            if (Card != null)
            {
                Card.ViewMode = ViewMode.Add;
                OnBeginInsert();
            }
            Page.Trace.Write("presenter", "BginInsert /");
        }


        public void EndInsert()
        {
            Page.Trace.Write("presenter", "EndInsert");
            try
            {
                if (Card != null)
                {
                    OnEndInsert();

                    Card.ViewMode = ViewMode.View;

                    RebindData = true;
                }

                Current = default(TEntity);
            }
            catch (UserMessage exp)
            {
                ShowUserMessage(exp);
            }
            Page.Trace.Write("presenter", "EndInsert /");
        }

      
        protected virtual void OnInitControls()
        {

        }

        protected virtual void OnInit()
        {

        }

        protected virtual void OnLoad()
        {

        }

        protected virtual void OnDataBind()
        {

        }

        public virtual new object Select()
        {
            Page.Trace.Write("presenter", "Select");
            var ret = Table.GetManyPaged(Pager,  Filter);
            Page.Trace.Write("presenter", "Select /");
            return ret;
        }

        public virtual new object SelectOne()
        {
            Page.Trace.Write("presenter", "Select");
            var ret = Table.GetOne(Filter);
            Page.Trace.Write("presenter", "Select /");
            return ret;
        }
       
        public virtual void OnDelete()
        {
            Page.Trace.Write("presenter", "OnDelete");
            Table.Remove(Current);
            Page.Trace.Write("presenter", "OnDelete /");
        }

        protected virtual void OnBeginUpdate()
        {
            Page.Trace.Write("presenter", "OnBeginUpdate");
            Current = GetCurrent();
            OnBeginUpdate_InitCard(Current);
            OnBeginInsert_AfterInitCard(Current);
            Page.Trace.Write("presenter", "OnBeginUpdate /");
        }

        protected virtual void OnEndUpdate()
        {
            Page.Trace.Write("presenter", "OnEndUpdate");
            TEntity entity = Current;
            Card.Fill(entity);
            OnBeforeUpdate(entity);
            OnUpdate(entity);
            OnAfterUpdate(entity);
            Page.Trace.Write("presenter", "OnEndUpdate /");
        }

        protected virtual void OnAfterUpdate(TEntity entity)
        {

        }

        protected virtual void OnBeforeUpdate(TEntity entity)
        {

        }

        protected virtual void OnBeforeInsert(TEntity entity)
        {

        }
        protected virtual void OnBeginInsert()
        {
            Page.Trace.Write("presenter", "OnBeginInsert");
            TEntity entity = Activator.CreateInstance<TEntity>();
            Current = entity;
            OnBeginInsert_InitCard(Current);
            OnBeginInsert_AfterInitCard(Current);
            
            Page.Trace.Write("presenter", "OnBeginInsert /");
        }

        public virtual void OnBeginInsert_InitCard(TEntity entity)
        {
             Card.Init(entity);
        }
        public virtual void OnBeginInsert_AfterInitCard(TEntity entity)
        {
            
        }
        
        public virtual void OnBeginUpdate_InitCard(TEntity entity)
        {
             Card.Init(entity);
        }

        public virtual void OnBeginUpdate_AfterInitCard(TEntity entity)
        {

        }
        protected virtual void OnEndInsert()
        {

            Page.Trace.Write("presenter", "OnEndInsert");

           // TEntity entity = Activator.CreateInstance<TEntity>();
            TEntity entity = Current;
            Card.Fill(entity);

            OnBeforeInsert(entity);
            OnInsert(entity);
            OnAfterInsert(entity);

            Page.Trace.Write("presenter", "OnEndInsert /");
        }

        protected virtual void OnInsert(TEntity entity)
        {
            Page.Trace.Write("presenter", "OnInsert");
            Table.Add(entity);
            Page.Trace.Write("presenter", "OnInsert /");
        }
        protected virtual void OnUpdate(TEntity entity)
        {
            Page.Trace.Write("presenter", "OnUpdate");
            Table.Save(entity);
            Page.Trace.Write("presenter", "OnUpdate /");
        }

        protected virtual void OnAfterInsert(TEntity entity)
        {

        }

        public static string GetCoockie(string name)
        {
            var reqCoockie = HttpContext.Current.Request.Cookies[name];
            if (reqCoockie != null)
            {
                if (!string.IsNullOrEmpty(reqCoockie.Value))
                {

                    return reqCoockie.Value;
                }
                if (reqCoockie.Values.Count > 0 && !string.IsNullOrEmpty(reqCoockie.Values[0]))
                {

                    return reqCoockie.Values[0];
                }
            }


            var resCoockie = HttpContext.Current.Response.Cookies[name];
            if (resCoockie != null)
            {
                if (!string.IsNullOrEmpty(resCoockie.Value))
                {

                    return resCoockie.Value;
                }
                if (resCoockie.Values.Count > 0 && !string.IsNullOrEmpty(resCoockie.Values[0]))
                {


                    return resCoockie.Values[0];
                }
            }

            
            


            return null;
        }

        public static string SetCoockie(string name, object value)
        {
            var setCookie = new HttpCookie(name, string.Format("{0}", value));

            setCookie.Expires = DateTime.MaxValue;


            HttpContext.Current.Response.Cookies.Add(setCookie);

            return setCookie.Value;
        }

        public static string GetQuery(string name)
        {
            try
            {
                var ret = HttpContext.Current.Request.QueryString[name];
                return ret;
            }
            catch { }
            return null;
        }

        public static T GetQuery<T>(string name)
        {
            return Convertor.Cast<T>(GetQuery(name));
        }


        protected static T GetSettingsValue<T>(string p)
        {
            return Convertor.Cast<T>( ConfigurationManager.AppSettings[p] );
        }

        public static void WebRedirect(string url)
        {
            HttpContext.Current.Response.Redirect(url);
        }





        public virtual void OnBginInsertInline_Init(TEntity entity)
        {

        }
        public virtual void BginInsertInline()
        {
            Page.Trace.Write("presenter", "OnBeginInsert");
            TEntity entity = Activator.CreateInstance<TEntity>();
            Current = entity;
            OnBginInsertInline_Init(entity);
            Page.Trace.Write("presenter", "OnBeginInsert /");
        }



        


        public virtual void EndInsertInline()
        {
            Page.Trace.Write("presenter", "OnEndInsert");

            
            TEntity entity = Current;
            
            OnBeforeInsert(entity);
            OnInsert(entity);
            OnAfterInsert(entity);

            Page.Trace.Write("presenter", "OnEndInsert /");
            
        }


        public object GetCurrentItem(bool withData)
        {
            if (withData)
                return Table.GetOne(Current);
            else
                return Current;
        }







        public void BeginUpdateInline()
        {
            Page.Trace.Write("presenter", "BginUpdate");
            OnBeginUpdateInline();
            Page.Trace.Write("presenter", "BginUpdate /");
        }
        public void EndUpdateInline()
        {
            Page.Trace.Write("presenter", "EndUpdate");
            OnEndUpdateInline();
            RebindData = true;
            Current = default(TEntity);
            Page.Trace.Write("presenter", "EndUpdate /");
        }


        protected virtual void OnBeginUpdateInline()
        {
            Page.Trace.Write("presenter", "OnBeginUpdate");
            Current = GetCurrent();
            Page.Trace.Write("presenter", "OnBeginUpdate /");
        }

     
        protected virtual void OnEndUpdateInline()
        {
            Page.Trace.Write("presenter", "OnEndUpdate");
            TEntity entity = Current;
            OnBeforeUpdate(entity);
            OnUpdate(entity);
            OnAfterUpdate(entity);
            Page.Trace.Write("presenter", "OnEndUpdate /");
        }


    }
    public abstract class Presenter<TEntity, TFilterEntity, TView, TListView, TFilter> :
       Presenter
       <
           TEntity,
           TFilterEntity,
           TView,
           TListView,
           TFilter,
           ICard<TEntity>

       >
        where TView : IView
        where TListView : IListView
        where TFilter : IFilter<TFilterEntity>
    {



    }

    public abstract class Presenter<TEntity, TFilterEntity, TView, TListView> :
       Presenter
       <
           TEntity,
           TFilterEntity,
           TView,
           TListView,
           IFilter<TFilterEntity>,
           ICard<TEntity>

       >
        where TView : IView
        where TListView : IListView
    {



    }


    public abstract class Presenter<TEntity, TFilterEntity, TView> :
       Presenter
       <
           TEntity,
           TFilterEntity,
           TView,
           IListView,
           IFilter<TFilterEntity>,
           ICard<TEntity>

       > where TView : IView
    {



    }

    public abstract class Presenter<TEntity, TFilterEntity> :
        Presenter
        <
            TEntity,
            TFilterEntity,
            IView,
            IListView,
            IFilter<TFilterEntity>,
            ICard<TEntity>

        >
    {



    }

    public abstract class Presenter<TEntity> :
        Presenter
        <
            TEntity,
            TEntity,
            IView,
            IListView,
            IFilter<TEntity>,
            ICard<TEntity>

        >
    {



    }
}
