﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
//using Microsoft.Reporting.WebForms;
using Dims.DAL;
using Dims.Web.Common;
using Dims.Web.Controls;

namespace Dims.Web.Controls
{



    public abstract class ReportsPresenter : ReportsPresenter<object, object, IView>
    {

    }
    public abstract class ReportsPresenter<TFilterEntity> : ReportsPresenter<TFilterEntity, object, IView>
    {

    }


    public abstract class ReportsPresenter<TFilterEntity, TEntity> : ReportsPresenter<TFilterEntity, TEntity , IView >
    {

    }

    public abstract class ReportsPresenter<TFilterEntity, TEntity, TView> : Presenter<TEntity, TFilterEntity, TView>, IReportPresenter where TView:IView
    {

        private IReportView _ReportView;
        public IReportView ReportView
        {

            get
            {
                return _ReportView;
            }
        }

        public void InitReportView(IReportView reportView)
        {
            _ReportView = reportView;

        }

        public virtual object GetParams()
        {
            return Filter;
        }


        public override void DataBind()
        {
            base.DataBind();

            if (FilterApplayed)
            {
                RebindReportData = true;
            }

            if (RebindReportData && ReportView != null &&  ReportView.Visible)
            {
                ReportView.DataBind();
            }
            
           
        }
        

        public bool RebindReportData { get; set; }
    } 

    

}
