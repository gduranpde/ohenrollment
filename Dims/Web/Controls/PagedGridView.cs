﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.ComponentModel;
using System.Collections.Specialized;
using Dims.Web.Common;


namespace Dims.Web.Controls
{
    public interface IListView
    {

        void DataBind();
        bool Visible { get; set; }
        //List<IOrderedDictionary> SelectedItemsKeys{get ;}

        //ViewMode ViewMode { get; set; }


        void ShowColumn(string columnName, bool show);
    }
    public class GridColum : Control, ITemplate
    {

        #region ITemplate Members

        public void InstantiateIn(Control container)
        {
            container.Controls.Add(this);
        }

        #endregion
    }

    public class PagedGridView :GridView , IPager  ,  IListView
    {
        public PagedGridView()
        {

           
        }
        
        void Page_InitComplete(object sender, EventArgs e)
        {
            Presenter.InitView(this);
            Presenter.InitPager(this);
        }


        private IPresenter _Presenter;
        public IPresenter Presenter
        {
            get
            {
                if (_Presenter == null && NamingContainer!=null)
                {
                    
                    _Presenter = (IPresenter)this.NamingContainer.FindControl(PresenterID); //(IPresenter) this.GetDataSource(); 
                }
                return _Presenter;
            }
        }

        [IDReferenceProperty(typeof(IPresenter))]
        public string PresenterID { get; set;} 

        protected override void OnRowEditing(GridViewEditEventArgs e)
        {
            e.Cancel = true;
            var dataKeys = DataKeys[e.NewEditIndex].Values;
                        
            Presenter.SetCurrent(dataKeys.Keys , dataKeys.Values );
            Presenter.BginUpdate();
        }
        protected override void OnRowDeleting(GridViewDeleteEventArgs e)
        {
            var dataKeys = DataKeys[e.RowIndex].Values;

            Presenter.SetCurrent(dataKeys.Keys, dataKeys.Values);
            Presenter.Delete();
        }
       

        protected override void OnRowCommand(GridViewCommandEventArgs e)
        {
           

            switch (e.CommandName)
            {
                case "Add":
                  
                   Presenter.BeginInsert();
                   return;
                break;


               /* case "Del":

                    Presenter.Delete();
                    return;
                    
                break;*/

            }
            
            Presenter.OnCommand(e.CommandName,e.CommandArgument);

            base.OnRowCommand(e);
        }

       
        private ITemplate _templEditColumn;

        [Browsable(false),
         DefaultValue(null),
         Description("Edit Column"),
         PersistenceMode(PersistenceMode.InnerProperty)]
        public ITemplate EditColumn
        {
            get { return _templEditColumn; }
            set { _templEditColumn = value; }
        }
        private ITemplate _templEditHeader;

        [Browsable(false),
         DefaultValue(null),
         Description("Add Header"),
         PersistenceMode(PersistenceMode.InnerProperty)]
        public ITemplate AddHeader
        {
            get { return _templEditHeader; }
            set { _templEditHeader = value; }
        }

        private ITemplate _templDeleteColumn;

        [Browsable(false),
         DefaultValue(null),
         Description("Delete Column"),
         PersistenceMode(PersistenceMode.InnerProperty)]
        public ITemplate DeleteColumn
        {
            get { return _templDeleteColumn; }
            set { _templDeleteColumn = value; }
        }
      /*  private ITemplate _templSelectHeader;

        [Browsable(false),
         DefaultValue(null),
         Description("Delete Header"),
         PersistenceMode(PersistenceMode.InnerProperty)]
        public ITemplate DeleteHeader
        {
            get { return _templSelectHeader; }
            set { _templSelectHeader = value; }
        }*/
        private string _CommandsClass;

        public string CommandsClass
        {
            get { return _CommandsClass; }
            set { _CommandsClass = value; }
        }


        private bool _AutoGenerateAddCommand = true;

        public bool AutoGenerateAddCommand
        {
            get { return _AutoGenerateAddCommand; }
            set { _AutoGenerateAddCommand = value; }
        }
        private bool _AutoGenerateEditCommand = true;

        public bool AutoGenerateEditCommand
        {
            get { return _AutoGenerateEditCommand; }
            set { _AutoGenerateEditCommand = value; }
        }
        private bool _AutoGenerateSelectCommand = true;

        public bool AutoGenerateSelectCommand
        {
            get { return _AutoGenerateSelectCommand; }
            set { _AutoGenerateSelectCommand = value; }
        }
        private bool _AutoGenerateDeleteCommand = true;

        public bool AutoGenerateDeleteCommand
        {
            get { return _AutoGenerateDeleteCommand; }
            set { _AutoGenerateDeleteCommand = value; }
        }


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);


            if (AutoGenerateDeleteCommand )
            {

                TemplateField tmp = new TemplateField();
                if(AutoGenerateDeleteCommand)
                tmp.ItemTemplate = DeleteColumn;
                             
                tmp.ItemStyle.CssClass = CommandsClass;
                tmp.HeaderStyle.CssClass = CommandsClass;
                Columns.Insert(0, tmp);
            }
            
            if (AutoGenerateAddCommand || AutoGenerateEditCommand)
            {

                TemplateField tmp2 = new TemplateField();
                
                if (AutoGenerateEditCommand)
                tmp2.ItemTemplate = EditColumn;
                if(AutoGenerateAddCommand)
                tmp2.HeaderTemplate = AddHeader;
                
                
                tmp2.ItemStyle.CssClass = CommandsClass;
                tmp2.HeaderStyle.CssClass = CommandsClass;
                Columns.Insert(1, tmp2);
            }


            


            Page.InitComplete += new EventHandler(Page_InitComplete);

            
                  
          
        }



        private List<GridViewRow> _SelectedRows;
        public List<GridViewRow> SelectedRows
        {
            get
            {
                
                if (_SelectedRows == null)
                {
                    _SelectedRows = new List<GridViewRow>();

                    foreach(GridViewRow row in Rows)
                    {
                        
                        CheckBox ch = (CheckBox)row.FindControl("ChSelect");
                        if (ch.Checked)
                            _SelectedRows.Add(row);
                    }
                }

                return _SelectedRows;
            }
        }


        private List<IOrderedDictionary> _SelectedItemsKeys;
        public List<IOrderedDictionary> SelectedItemsKeys
        {
            get
            {
                
                if (_SelectedItemsKeys == null)
                {
                    _SelectedItemsKeys = new List<IOrderedDictionary>();

                    try
                    {

                        foreach (GridViewRow row in SelectedRows)
                        {

                            _SelectedItemsKeys.Add(DataKeys[row.DataItemIndex].Values);
                        }
                    }
                    catch { }
                }
                return _SelectedItemsKeys;
            }
        }


        public new  int PageIndex
        {

            get 
            {
                try
                {
                    return (int)ViewState["Pager:PageIndex"];
                }
                catch 
                {
                    return 0;
                }
            }
            set 
            {

                if (PageIndex != value)
                {
                    Presenter.RebindData = true;
                }
                ViewState["Pager:PageIndex"] = value; 
            }
        }

        public new int PageSize
        {
            get
            {
                try
                {
                    return (int)ViewState["Pager:PageSize"];
                }
                catch { return 20;  }
            }
            set
            {
                /*if (PageSize != value && Presenter!=null)
                {
                    Presenter.RebindData = true;
                }*/

                ViewState["Pager:PageSize"]= value;
            }
        }


        public  int TotalCount
        {
            get 
            {
                try
                {
                    return (int)ViewState["Pager:TotalCount"];
                }
                catch
                {
                    return 0;
                }
            }
            set 
            {
                ViewState["Pager:TotalCount"] = value;
            }
        }

        public bool IsServer
        {
            get
            {
                return true;
            }

        }

        public override int PageCount
        {
            get
            {
                return (int)Math.Ceiling( (double) TotalCount / PageSize) ;
            }
        }

      
        void IListView.DataBind()
        {
            this.DataSource = Presenter.Select();

            try
            {
                base.PageSize = ((ICollection)this.DataSource).Count;
            }
            catch { }
            
            this.PagerSettings.Visible = PageCount > 1;
            

            
            this.DataBind();
         
            
        }



        #region IListView Members


        public void ShowColumn(string columnName, bool show)
        {
            
        }

        #endregion
    }


}
