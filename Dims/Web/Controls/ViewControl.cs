﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Dims.Web.Common;
using System.Web.UI;

namespace Dims.Web.Controls
{


    public class ViewControl : DetailsView, IListView, IViewControlDataSource
    {

        ObjectDataSource ods;

        private IPresenter _Presenter;
        public IPresenter Presenter
        {
            get
            {
                if (_Presenter == null && NamingContainer != null)
                {

                    _Presenter = (IPresenter)this.NamingContainer.FindControl(PresenterID); //(IPresenter) this.GetDataSource(); 
                }
                return _Presenter;
            }
        }

        [IDReferenceProperty(typeof(IPresenter))]
        public string PresenterID { get; set; }


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Page.InitComplete += new EventHandler(Page_InitComplete);

            var t = typeof(IViewControlDataSource);
            ods = new ObjectDataSource(t.FullName, "SelectObject");
            ods.ObjectCreating += new ObjectDataSourceObjectEventHandler(ods_ObjectCreating);

            this.ModeChanging += new DetailsViewModeEventHandler(ViewControl_ModeChanging);
        }

        void ViewControl_ModeChanging(object sender, DetailsViewModeEventArgs e)
        {
            switch (e.NewMode)
            {
                case DetailsViewMode.Edit:

                    Presenter.SetCurrent(DataKey.Values.Keys, DataKey.Values.Values);
                    Presenter.BginUpdate();
                break;
            }
        }



        void ods_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = this;
        }


        public object SelectObject()
        {
            return Presenter.SelectOne();
        }



        void Page_InitComplete(object sender, EventArgs e)
        {
            Presenter.InitView(this);
        }

        void IListView.DataBind()
        {
            this.DataSource = null;
            this.DataSource = ods; 
            this.DataBind();
         
        }

        bool IListView.Visible
        {
            get
            {
                return Visible;
            }
            set
            {
                Visible = value;
            }
        }








        #region IListView Members


        public void ShowColumn(string columnName, bool show)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public interface IViewControlDataSource
    {
         object SelectObject();
    }
    /*private class ViewControlDataSource : IViewControlDataSource
    {

        object SelectObject()
        {
            return null;
        }
    }*/
}
