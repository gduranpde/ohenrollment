﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dims.Web.Common;

namespace Dims.Web.Controls
{
    public interface ICard
    {
        ViewMode ViewMode { get; set; }

        void Ok();
        void Cancel();

        event EventHandler OnOk;
        event EventHandler OnCancel;
        event EventHandler ViewMode_Changed;

    }
    public interface ICard<TEntity> : ICard
    {
        void Init(TEntity entity);
        void Fill(TEntity entity);


    }
    public abstract class CardControl<TEntity> : CardControl<TEntity, IPresenter>
    {

    }
    public abstract class CardControl<TEntity, TPresenter> : ControlWithPresenter<TPresenter>, ICard<TEntity> where TPresenter : IPresenter
    {
        public CardControl()
        {
            
        }

        protected virtual void InitCard()
        {
           
                Presenter.InitCard(this);
            
        }
        protected override void OnInit(EventArgs e)
        {
            if (!IsPostBack)
            {
                InitData();
            }
            if (Presenter != null)
            {
                InitCard();
            }
            Page.InitComplete += new EventHandler(Page_InitComplete);
            
            base.OnInit(e);
        }

        protected virtual void InitData()
        {

        }

        protected override void OnLoad(EventArgs e)
        { 
            
            if (!IsPostBack)
            {
                if (ViewMode == ViewMode.View)
                {
                    Visible = false;
                }
            }
            
            
            base.OnLoad(e);
        }


        void Page_InitComplete(object sender, EventArgs e)
        {
            
        }


        public event EventHandler ViewMode_Changed;
   
        private ViewMode? _ViewMode;
        public ViewMode ViewMode
        {
            get
            {
                if (_ViewMode == null)
                {
                    try
                    {
                        _ViewMode = (ViewMode)ViewState["ViewMode"];
                    }
                    catch { }
                }

                return (ViewMode) (_ViewMode?? ViewMode.View);
            }
            set
            {
                _ViewMode = value;

                this.Visible = (value != ViewMode.View);
                
                ViewState["ViewMode" ] = value;

                if (ViewMode_Changed != null)
                {
                    ViewMode_Changed(this, null);
                }
            }
        }


        public bool get_IsValid()
        {
            
            if (Page.Validators!= null)
            {
                ValidatorCollection validators = Page.Validators;
                int count = validators.Count;
                for (int i = 0; i < count; i++)
                {
                    if (!validators[i].IsValid)
                    {
                        return false;
                    }
                }
            }
            return true;
        }



        public virtual void Ok()
        {
            var vv = get_IsValid();
            if (OnOk != null && Page.IsValid)
            {
                OnOk(this, null);
            }
        }

        public virtual void Cancel()
        {
            if (OnCancel != null)
            {
                OnCancel(this, null);
            }
        }

        public event EventHandler OnOk;

        public event EventHandler OnCancel;

        public abstract new void Init(TEntity entity);

        public abstract void Fill(TEntity entity);
    }

    
}
