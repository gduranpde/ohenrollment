﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.Web.Common;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace Dims.Web.Controls
{
    public abstract class FilterControl : FilterControl<object, IPresenter>
    {
        public override void Fill(object filter) { }

        public override void Init(object filter) { }

    }

    public abstract class FilterControl<TFilterEntity> : FilterControl<TFilterEntity, IPresenter>
    {

    }


    public abstract class FilterControl<TFilterEntity, TPresenter> : ControlWithPresenter<TPresenter>, IFilter<TFilterEntity> where TPresenter : IPresenter
    {



        public event EventHandler Apply;

        protected IButtonControl BtnFind;

        protected override void OnInit(EventArgs e)
        {
            //if (!IsPostBack)
            //{

            //    InitData();
            //}

            //this.Controls.Add(FilterSkin);

            Presenter.InitFilterer(this);

            if (BtnFind != null)
                BtnFind.Click += new EventHandler(BtnFind_Click);

            base.OnInit(e);
        }



        protected void BtnFind_Click(object sender, EventArgs e)
        {
            ApplyFilter();
        }

     /*   protected void BtnFind_Click(object sender, ImageClickEventArgs e)
        {
            ApplyFilter();
        }*/

        public abstract void Fill(TFilterEntity filter);

        public abstract new void Init(TFilterEntity filter);

        public virtual void InitData() { }

        protected virtual void ApplyFilter()
        {
            if (Apply != null)
                Apply(this, null);
        }
    }
}
