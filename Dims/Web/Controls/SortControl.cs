﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.Web.Common;
using System.Web.UI;
using System.ComponentModel;
using System.Collections;
using System.Collections.Specialized;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Dims.DAL;

namespace Dims.Web.Controls
{

    


   /* public abstract class SortControl : SortControl<object, IPresenter>
    {
        public override void Fill(object filter) { }

        public override void Init(object filter) { }

    }

    public abstract class SortControl<TFilterEntity> : SortControl<TFilterEntity, IPresenter>
    {

    }*/

    [Themeable(true)]
    [SupportsEventValidation]
    [ParseChildren(true)]
    [PersistChildren(true)]
    public  class SortControl : ControlWithPresenter<IPresenter>, ISort
    {
        public SortControl()
        {
           
        }

        public event EventHandler Apply; 

       

        [TemplateContainer(typeof(ContentHolder)),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateInstance(TemplateInstance.Single),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ITemplate ItemTemplate { get; set; }

        [TemplateContainer(typeof(ContentHolder)),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateInstance(TemplateInstance.Single),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ITemplate FooterTemplate { get; set; }

        [TemplateContainer(typeof(ContentHolder)),
        PersistenceMode(PersistenceMode.InnerProperty),
        TemplateInstance(TemplateInstance.Single),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ITemplate HeaderTemplate { get; set; }
        
        
        
        [DefaultValue("")]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public List<SortItem> Items { get; set; }

        public string ItemID { get; set; }
        public string ItemCss { get; set; }
        public string ItemCssAsc { get; set; }
        public string ItemCssDesc { get; set; }

        public string SortProperty { get; set; }

        protected string SortExpression
        {
            get
            {
                return Convertor.ToString(ViewState["SortExpression"]);
            }
            set
            {
                ViewState["SortExpression"] = value;
            }
        }

        protected string SortItemCurrent
        {
            get
            {
               
                try
                {
                    if (SortExpression != null)
                    {
                        var index = SortExpression.LastIndexOf(' ');
                        return SortExpression.Substring(0, index).Trim();
                    }
                }
                catch{ }

                return null;
            }
        }

        private string SortItemDeirection
        {
            get
            {

                try
                {
                    if (SortExpression != null)
                    {
                        var index = SortExpression.LastIndexOf(' ');
                        return SortExpression.Substring(index).Trim();
                    }
                }
                catch { }

                return null;
            }
        }


        protected override void OnInit(EventArgs e)
        {
            
            Presenter.InitSortControl(this);
            base.OnInit(e);
            
            
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            CreateContents();
        }

        protected override void OnPreRender(EventArgs e)
        {
            CreateContents();
            base.OnPreRender(e);


        }


        public void Fill(object filter)
        {
            Reflector.SetProperty(filter, SortProperty, SortExpression);
        }

        public new void Init(object filter)
        {
            SortExpression = Reflector.GetPropertyValue<string>(filter, SortProperty);
        }

        public virtual void InitData() { }

        protected void ApplySort()
        {
            if (Apply != null)
                Apply(this, null);
        }
       


        private void CreateContents()
        {

            this.Controls.Clear();

            var HeaderContainer = new ContentHolder();
            HeaderTemplate.InstantiateIn(HeaderContainer);
            this.Controls.Add(HeaderContainer);

            if (Items!=null)
            foreach(SortItem item in Items)
            { 
                if (ItemTemplate != null )
                {
                    
                    var ItemContainer = new ContentHolder();
                    ItemTemplate.InstantiateIn(ItemContainer);

                    var bt = ItemContainer.FindControl(ItemID) as IButtonControl;
                    var wbt = ItemContainer.FindControl(ItemID) as WebControl;
                    if (bt != null && wbt!=null)
                    {
                        wbt.CssClass = ItemCss;
                        bt.Text = item.Text;
                        bt.CommandName = item.Value;
                        bt.CommandArgument = "ASC";
                        bt.Command += new CommandEventHandler(bt_Command);
                        

                        if (bt.CommandName==SortItemCurrent)
                        {
                            if (SortItemDeirection == "ASC")
                            {
                                bt.CommandArgument = "DESC";
                                wbt.CssClass = ItemCssAsc;
                            }
                            else
                            {
                                bt.CommandArgument = "ASC";
                                wbt.CssClass = ItemCssDesc;
                            }
                        }

                    }

                    this.Controls.Add(ItemContainer);
                }
            }

            var FooterContainer = new ContentHolder();
            FooterTemplate.InstantiateIn(FooterContainer);
            this.Controls.Add(FooterContainer);

        }

        void bt_Command(object sender, CommandEventArgs e)
        {
            SortExpression = string.Format("{0} {1}", e.CommandName,e.CommandArgument).Trim();

            ApplySort();
        }

       

        


    }

    public class SortItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
        
    }
}
