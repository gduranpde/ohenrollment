using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
namespace Dims.DAL
{
    public class StoredProcedure:IChild<DataBase>
    {

        public int CommandTimeout = -1;

        public StoredProcedure(object obj)
        {

        }
        public StoredProcedure()
        {
            
                SP sp = Reflector.GetClassAttribute<SP>(this);
                if (sp != null)
                {
                    _Name = sp.Name;
                }
                else
                {
                    _Name = this.GetType().Name;
                }

                SQL sql = Reflector.GetClassAttribute<SQL>(this);
              
                if (sql != null)
                {  _Text = sql.Text;
                    CommandType = CommandType.Text;
                }
            
                _MapParams = Reflector.GetClassAttribute<Map>(this);

                if (_MapParams == null)
                    _MapParams = new Map("*");

                spTimeOut tmieout = Reflector.GetClassAttribute<spTimeOut>(this);
                if (tmieout != null)
                {
                    CommandTimeout = tmieout.Value;
                }
           
        }

        private  string _Name;
        public string Name
        {
            get 
            {
                return _Name;
            }

        }

       

        private Map _MapParams = null;
        public Map MapParams
        {
            get
            {
                return _MapParams;
            }
        }

        
        private DataBase _db;
        public DataBase db
        {
            get
            {
                return _db;
            }
        }

        
        public void SetParent(DataBase p)
        { 
            _db = p;
        }

       

        public object Entity
        {
            set
            {
                FillFromEntity(value);
            }
            
        }
        public void FillFromEntity(params object [] Entity)
        {
            if (Entity != null)
            {
                foreach (object o in Entity)
                {

                    MapParams.FromEntityToSP(o, this);

                }
            }
                
        }
        public void FillEntity(params object[] Entity)
        {
            if (Entity != null)
                foreach (object o in Entity)
                    MapParams.FromSPToEntity( o , this);
        }


        private string _Text;
        public string Text
        {
            get
            {
                return _Text;
            }
            set
            {
                _Text = value;
            }
        }

        public CommandType CommandType = CommandType.StoredProcedure;

        public string CommandText
        {
            get
            {
                return (CommandType == CommandType.StoredProcedure) ? Name : Text;

            }
        }


        public DbCommand CurCommand;


        
    }
}
