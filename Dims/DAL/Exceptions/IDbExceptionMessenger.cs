using System;
using System.Collections.Generic;
using System.Text;

namespace Dims.DAL
{
    public interface IDbExceptionMessenger
    {
        string ParseException(int code);
    }
}
