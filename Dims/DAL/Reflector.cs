using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Collections.Specialized;
using System.Collections;


namespace Dims.DAL
{
    public static class Reflector
    {
        public static List<T> GetTypedFields<T>(object parent)
        {
            if (parent == null)
                return null;

            Type type = parent.GetType();

            List<T> ret = new List<T>();


            foreach (FieldInfo f in type.GetFields(BindingFlags.GetProperty | BindingFlags.GetField | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                try
                {

                    object val = f.GetValue(parent);
                    if (val is T)
                        ret.Add((T)val);
                }
                catch { }

            }



            return ret;
        }




        public static List<FieldWithAttributesInfo<TAttribute>> GetFieldsByAttribute<TAttribute>(Type obj,Type attr)
        {
           
            //Type type = parent.GetType();
            List<FieldWithAttributesInfo<TAttribute>> ret = new List<FieldWithAttributesInfo<TAttribute>>();

            
            foreach (FieldInfo f in obj.GetFields())
            {
                try
                {

                    object[] attributes = f.GetCustomAttributes(attr, true);
                    if (attributes != null && attributes.Length>0)
                    {
                        FieldWithAttributesInfo<TAttribute> finfo = new FieldWithAttributesInfo<TAttribute>();
                        finfo.FieldInfo = f;
                        finfo.Attribute = (TAttribute)attributes[0];
                        ret.Add(finfo);
                    }
                }
                catch (Exception exp)
                {
                    int i = 0;
                }

            }


            

            return ret;
        }

        public static List<PropertyWithAttributesInfo<TAttribute>> GetPropertiesByAttribute<TAttribute>(Type obj, Type attr)
        {

            //Type type = parent.GetType();
            List<PropertyWithAttributesInfo<TAttribute>> ret = new List<PropertyWithAttributesInfo<TAttribute>>();


            foreach (PropertyInfo f in obj.GetProperties())
            {
                try
                {

                    object[] attributes = f.GetCustomAttributes(attr, true);
                    if (attributes != null && attributes.Length > 0)
                    {
                        PropertyWithAttributesInfo<TAttribute> finfo = new PropertyWithAttributesInfo<TAttribute>();
                        finfo.PropertyInfo = f;
                        finfo.Attribute = (TAttribute)attributes[0];
                        ret.Add(finfo);
                    }
                }
                catch (Exception exp)
                {
                    int i = 0;
                }

            }




            return ret;
        }
        public static TAttribute GetPropertyAttribute<TAttribute>(PropertyInfo prop)
        {

               
              var ret = prop.GetCustomAttributes(typeof(TAttribute) , true);

              if (ret == null || ret.Length == 0)
                  return default(TAttribute);

              return (TAttribute)ret[0];  
                                
                
            
        }

        //public static Attribute GetClassAttribute(object obj,Type attr)
        //{

        //    if (obj == null)
        //        return null;

        //    Type type = obj.GetType();

        //    object [] ret =  type.GetCustomAttributes(attr,true);
        //    if (ret != null)
        //    {
        //        return (Attribute) ret[0];
        //    }
        //    return null;
        //}

        public static List<TAttribute> GetClassAttributes<TAttribute>(object obj)
        {
            List<TAttribute> ret = new List<TAttribute>();
            Type type = obj.GetType();
            object [] attr = type.GetCustomAttributes(typeof(TAttribute) ,true);

            if (attr != null)
            {
                foreach (TAttribute t in attr)
                {
                    ret.Add(t);

                }
            }
            return ret;
        }
        public static TAttribute GetClassAttribute<TAttribute>(object obj)
        {
            TAttribute ret = default(TAttribute);
            List<TAttribute> attr = GetClassAttributes<TAttribute>(obj);
            if(attr.Count>0)
                ret = attr[0];
            return ret;
        }


        public static TEntity EntityFromParams<TEntity>(IOrderedDictionary dic)
        {
            TEntity obj = Activator.CreateInstance<TEntity>();

            Type t = typeof(TEntity);
            foreach (string par in dic.Keys)
            {
                try
                {
                    PropertyInfo pinfo = t.GetProperty(par);
                    pinfo.SetValue(obj, dic[par], null);
                }
                catch { }
            }

            return obj;

        }

        public static object EntityFromParams(Type t , IOrderedDictionary dic)
        {
            object obj = Activator.CreateInstance(t);

           
            foreach (string par in dic.Keys)
            {
                try
                {
                    PropertyInfo pinfo = t.GetProperty(par);
                    pinfo.SetValue(obj, dic[par], null);
                }
                catch { }
            }

            return obj;

        }
        public static object EntityFromParams(Type t, ICollection keys, ICollection values) 
        {
            if (keys == null || values == null)
                return null;
            object obj = Activator.CreateInstance(t);

            var ikeys = keys.GetEnumerator();
            var ivalues = values.GetEnumerator();
            while (ikeys.MoveNext() && ivalues.MoveNext())
            {
                var key = (string)ikeys.Current;
                var value = ivalues.Current;

                try
                {
                    PropertyInfo pinfo = t.GetProperty(key);
                    pinfo.SetValue(obj, value, null);
                }
                catch { }
            }
          

            return obj;

        }

        public static Type GetSPType<Entity, sp>(Table<Entity> Table, string Prefix, string Postfix , string Separator)
       {
           try
           {
               object spt = GetClassAttribute<sp>(Table);
               if (spt == null)
               {
                   Type t = Table.GetType();
                   string type = string.Format("{0}.{1}{4}{2}{4}{3}, {5}" , t.Namespace , Prefix , Table.TableName , Postfix ,Separator , t.Assembly.FullName);
                   return Type.GetType(type);


               }
               else
               {
                   return ((spType)spt).Type;
               }

           }
           catch { }

           return null;
       }


        public static T GetPropertyValue<T>(object p, string property)
        {
            T ret = default(T);
            if (p == null || string.IsNullOrEmpty(property))
                return ret;
            

            Type t = p.GetType();

            PropertyInfo prop = t.GetProperty(property);

            if (prop == null)
                return ret;


            try
            {
                ret =(T)prop.GetValue(p, null);

            }
            catch { }

            return ret;

        }

        public static object GetPropertyValue(object p, string property)
        {
            return GetPropertyValue<object>(p, property);

        }

        public static PropertyInfo []  GetProperties(Type type)
        {
            return type.GetProperties();
        }

      
        public static void UpdateEntityFromParams(object p, IDictionary values)
        {
            if (p == null)
                return;


            var type = p.GetType();

            foreach (DictionaryEntry val in values)
            {

                var prop = type.GetProperty(string.Format("{0}",val.Key));
                if (val.Value  ==  null )
                {

                    prop.SetValue(p, (prop.PropertyType.IsValueType ?  Activator.CreateInstance(prop.PropertyType) : null ) , null);
                }
                else
                {
                    prop.SetValue(p, Convert.ChangeType(val.Value, prop.PropertyType), null);
                }
            }
        }

        public static bool SetProperty(object obj, string PropertyName, object Value)
        {
            try
            {
                if (obj == null)
                    return false;

                Type t = obj.GetType();

                var prop = t.GetProperty(PropertyName);

                prop.SetValue(obj, Convert.ChangeType(Value, prop.PropertyType) , null);

                return true;
            }
            catch 
            {
                return false;
            }
            
        }

        public static bool InitProps(object obj,  object value)
        {
            if (obj == null)
                return false;
            try
            {
                var t = obj.GetType();

                var props = t.GetProperties();
                foreach (PropertyInfo p in props)
                {
                    if (p.PropertyType.IsInstanceOfType(value) && p.CanWrite)
                    {
                        p.SetValue(obj, value, null);
                    }
                }

                return true;
            }
            catch 
            {
                return false;
            }
            
        }
    }
}
