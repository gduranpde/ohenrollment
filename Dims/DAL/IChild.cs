using System;
using System.Collections.Generic;
using System.Text;

namespace Dims.DAL
{
    public interface IChild<TParent>
    {
        void SetParent(TParent p);

    }
}
