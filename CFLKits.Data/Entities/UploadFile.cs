﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CFLKits.Data.Entities
{
    public class UploadFile
    {
        public int UploadID { get; set; }
        public DateTime UploadDate { get; set; }
        public string FileName { get; set; }
        public string ReplaceOrAppend { get; set; }
        public int RecordsUploaded { get; set; }
        public string Exceptions { get; set; }
    }
}
