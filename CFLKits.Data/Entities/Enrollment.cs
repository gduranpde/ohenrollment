using System;

namespace CFLKits.Entities
{
    public class Enrollment
    {
        public Int64 EnrollmentID { get; set; }
        public DateTime SubmittedDate { get; set; }
        public string InvitationCode { get; set; }
        public string AccountNumber { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string ContactZipCode { get; set; }
        public string ShipToAddress { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string ReferralSource { get; set; }
        public string WaterHeaterFuel { get; set; }
        public string HeaterFuel { get; set; }
        public string KitSelection { get; set; }
        public string AccountNumber12 { get; set; }
        public string FormattedPhone { get; set; }
        public bool ReplicationStatus { get; set; }
        public bool? MoreInfo { get; set; }
        public string OperatingCompany { get; set; }
    }
}