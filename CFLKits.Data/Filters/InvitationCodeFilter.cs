﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CFLKits.Data.Filters
{
    public class InvitationCodeFilter
    {
        public string InvitationCode { get; set; }

        public string AccountNumber { get; set; }

        public string AccountNumber12 { get; set; }
    }
}
