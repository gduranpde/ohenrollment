﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CFLKits.Data.Filters
{
    public class EnrollmentDetails
    {
        public string ContactFirstName { get; set; }

        public string ContactLastName { get; set; }

        public string ContactPhone { get; set; }

        public string ContactZipCode { get; set; }

        public string ReferralSource { get; set; }

        public string WaterHeaterFuel { get; set; }

        public string HeaterFuel { get; set; }
    }
}