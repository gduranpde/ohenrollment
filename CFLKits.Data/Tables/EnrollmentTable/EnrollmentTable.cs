using System;
using System.Collections.Generic;
using System.Text;
using CFLKits.Data.Filters;
using CFLKits.Data.Tables;
using CFLKits.Entities;
using Dims.DAL;

namespace CFLKits.Data
{
    [spGetMany(typeof(sp_Enrollment_GetMany))]
    [spGetAll(typeof(sp_Enrollment_GetAll))]
    [spGetOne(typeof(sp_Enrollment_GetOne))]
    [spAdd(typeof(sp_Enrollment_Add))]
    [spSave(typeof(sp_Enrollment_Save))]
    [spRemove(typeof(sp_Enrollment_Remove))]
    [spUpdate(typeof(sp_UpdateReplicationStatus))]
    [spGetEnrollID(typeof(sp_GetLastEnrollmentID))]
    public class EnrollmentTable : Table<Enrollment>
    {
        public List<Enrollment> GetMany(EnrollmentFilter filter)
        {
            return db.Execute<Enrollment>(new sp_Enrollment_GetMany
            {
                AccountNumber = filter.AccountNumber,
                AccountNumber12 = filter.AccountNumber12,
                InvitationCode = filter.InvitationCode,
                FromDate = filter.FromDate,
                ToDate = filter.ToDate,
                ContactFirstName = filter.ContactFirstName,
                ContactLastName = filter.ContactLastName,
                ContactEmail = filter.ContactEmail
            });
        }

        public List<Enrollment> GetEnrollID(EnrollmentDetails filter)
        {
            return db.Execute<Enrollment>(new sp_GetLastEnrollmentID
            {
                ContactFirstName = filter.ContactFirstName,
                ContactLastName = filter.ContactLastName,
                ContactPhone = filter.ContactPhone,
                ContactZipCode = filter.ContactZipCode,
                ReferralSource = filter.ReferralSource,
                WaterHeaterFuel = filter.WaterHeaterFuel,
              
            });
        }
    }
}