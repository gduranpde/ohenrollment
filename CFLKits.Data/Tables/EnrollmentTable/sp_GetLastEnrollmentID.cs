﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;

namespace CFLKits.Data.Tables
{
    public class sp_GetLastEnrollmentID : StoredProcedure
    {
        [InParameter]
        public string ContactFirstName;

        [InParameter]
        public string ContactLastName;

        [InParameter]
        public string ContactPhone;

        [InParameter]
        public string ContactZipCode;

        [InParameter]
        public string ReferralSource;

        [InParameter]
        public string WaterHeaterFuel;

       
    }
}