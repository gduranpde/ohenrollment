﻿
using System;
using Dims.DAL;

namespace CFLKits.Data.Tables
{
    public class sp_Enrollment_GetMany : StoredProcedure
    {
        [InParameter]
        public string InvitationCode;

        [InParameter]
        public string AccountNumber;

        [InParameter]
        public string AccountNumber12;


        [InParameter] public DateTime? FromDate;

        [InParameter] public DateTime? ToDate;


        [InParameter]
        public string ContactFirstName;
        [InParameter]
        public string ContactLastName;

        [InParameter]
        public string ContactEmail;
    }
}
