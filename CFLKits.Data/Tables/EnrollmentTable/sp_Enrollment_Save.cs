using System;
using System.Text;
using System.Collections.Generic;

using Dims.DAL;


namespace  CFLKits.Data.Tables
{
    
    internal class sp_Enrollment_Save:StoredProcedure
    {       
        [InParameter]
        public Int64 EnrollmentID;
        [InParameter]
        public DateTime SubmittedDate;
        [InParameter]
        public string InvitationCode;
        [InParameter]
        public string AccountNumber;
        [InParameter]
        public string ContactFirstName;
        [InParameter]
        public string ContactLastName;
        [InParameter]
        public string ContactEmail;
        [InParameter]
        public string ContactPhone;
        [InParameter]
        public string ContactZipCode;
        [InParameter]
        public string ShipToAddress;
        [InParameter]
        public string Address1;
        [InParameter]
        public string Address2;
        [InParameter]
        public string City;
        [InParameter]
        public string State;
        [InParameter]
        public string Zip;
        [InParameter]
        public string ReferralSource;
        [InParameter]
        public string WaterHeaterFuel;
        [InParameter]
        public string HeaterFuel;
        [InParameter]
        public string KitSelection;
        [InParameter]
        public string AccountNumber12;
        [InParameter]
        public bool? MoreInfo;
        [InParameter]
        public string OperatingCompany;
    }
}