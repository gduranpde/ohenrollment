﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dims.DAL;

namespace CFLKits.Data.Tables
{
    internal class sp_UpdateReplicationStatus : StoredProcedure
    {
        [InParameter]
        public Int64 EnrollmentID;
    }
}