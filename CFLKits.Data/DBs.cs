
using System;
using System.Text;
using System.Collections.Generic;
using CFLKits.Data;

namespace CFLKits.Data
{
	public class DataProvider
	{
		    [ThreadStatic]
        	private static CFLKitsDb _Current ;

        	public static CFLKitsDb Current
        	{
			get
			{
				if(_Current  == null)
				{
					_Current = new CFLKitsDb ();
				}
				return _Current;
			}
		}
	}
}
