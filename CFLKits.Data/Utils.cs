﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CyberianSoft.Common;
using CyberianSoft.Common.Attributes;

namespace CFLKits.Data
{
    public class Utils
    {
        public static EnumListItem[] EnumToListItemsNoSort(Type tEnum)
        {
            List<Enum> val = new List<Enum>();
            foreach (Enum en in Enum.GetValues(tEnum))
            {
                val.Add(en);
            }
            return EnumToListItemsNoSort(val.ToArray());
        }

        public static EnumListItem[] EnumToListItemsNoSort(params Enum[] values)
        {
            var ret = new List<EnumListItem>();
            //Comparison<Enum> comp = Desc.CompareByDesc;
            //Array.Sort(values, comp);

            foreach (Enum n in values)
            {
                EnumListItem item = EnumToListItem(n);

                if (item != null)
                    ret.Add(item);
            }


            return ret.ToArray();
        }

        protected static EnumListItem EnumToListItem(object e)
        {
            try
            {
                string text = EnumUtils.GetEnumDesc(e);

                return new EnumListItem(text, Convert.ToInt32(e), string.Format("{0}", e));
            }
            catch
            {
            }
            return null;
        }

        
    }
}
