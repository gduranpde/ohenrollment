﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EnrollmentCard.ascx.cs" Inherits="CFLKits.Web.Cards.EnrollmentCard" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:HiddenField  ID="HiddenField_Id" runat="server" />
<table>
    <tr>
        <td>
            SubmittedDate:
        </td>
          <td>
            <telerik:RadDatePicker ID="RadDatePicker_SubmittedDate" Runat="server" Skin="Telerik" Width="120px">
                <calendar skin="Telerik" usecolumnheadersasselectors="False" 
                    userowheadersasselectors="False" viewselectortext="x">
                </calendar>
                <datepopupbutton hoverimageurl="" imageurl="" />
                <dateinput dateformat="MM/dd/yyyy" displaydateformat="MM/dd/yyyy">
                </dateinput>
            </telerik:RadDatePicker>
        </td>       
    </tr>
    <tr>
         <td>
            InvitationCode:
        </td>
        <td>
            <asp:TextBox ID="txtInvitationCode" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr>
         <td>
            AccountNumber:
        </td>
        <td>
            <asp:TextBox ID="txtAccountNumber" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr>
         <td>
            AccountNumber New:
        </td>
        <td>
            <asp:TextBox ID="txtAccountNumber12" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr>
         <td>
            ContactFirstName:
        </td>
        <td>
            <asp:TextBox ID="txtContactFirstName" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr>
         <td>
            ContactLastName:
        </td>
        <td>
            <asp:TextBox ID="txtContactLastName" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr>
         <td>
            ContactEmail:
        </td>
        <td>
            <asp:TextBox ID="txtContactEmail" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr>
         <td>
            ContactPhone:
        </td>
        <td>
            <asp:TextBox ID="txtContactPhone" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr>
         <td>
            ContactZipCode:
        </td>
        <td>
            <asp:TextBox ID="txtContactZipCode" runat="server"></asp:TextBox>
        </td>
    </tr>
    
     <tr>
         <td>
            ShipToAddress:
        </td>
        <td>          
          <asp:RadioButtonList runat="server" ID="lstShippingOptions">
						<asp:ListItem Value="M">Mailing Address</asp:ListItem>
						<asp:ListItem Value="S">Service Address</asp:ListItem>
						<asp:ListItem Value="O">Other</asp:ListItem>
					</asp:RadioButtonList>
        </td>
    </tr>


    <tr>
         <td>
            Address1:
        </td>
        <td>
            <asp:TextBox ID="txtAddress1" runat="server"></asp:TextBox>
        </td>
    </tr>

    <tr>
         <td>
            Address2:
        </td>
        <td>
            <asp:TextBox ID="txtAddress2" runat="server"></asp:TextBox>
        </td>
    </tr>

    <tr>
         <td>
            City:
        </td>
        <td>
            <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
        </td>
    </tr>

    <tr>
         <td>
            State:
        </td>
        <td>
            <asp:TextBox ID="txtState" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
         <td>
            Zip:
        </td>
        <td>
            <asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
         <td>
            Referral Source:
        </td>
        <td>
           <asp:DropDownList ID="ddlReferralSources" runat="server">                                             
                        <asp:ListItem Value="Direct mail">Direct mail</asp:ListItem>                        
                        <asp:ListItem Value="Bill Insert">Bill Insert</asp:ListItem>
                         <asp:ListItem Value="Press Release">Press Release</asp:ListItem>
                        <asp:ListItem Value="Friend">Friend</asp:ListItem>
                        <asp:ListItem Value="Don’t Recall">Don’t Recall</asp:ListItem>
                        <asp:ListItem Value="Email">Email</asp:ListItem>              
                        <asp:ListItem Value="Newspaper">Newspaper</asp:ListItem>         
                        <asp:ListItem Value="Other">Other</asp:ListItem>
           </asp:DropDownList>
        </td>
    </tr>
    <tr>
         <td>
            Water Heater Fuel:
        </td>
        <td>
           <asp:DropDownList ID="ddlWaterHeaterFuel" runat="server" OnSelectedIndexChanged="ddlWaterHeaterFuel_OnSelectedIndexChanged" AutoPostBack="true">                       
                        <asp:ListItem Value="Electric">Electric</asp:ListItem>
                        <asp:ListItem Value="Non-electric">Non-electric</asp:ListItem>
           </asp:DropDownList>
        </td>
    </tr>
    <tr>
           <td>Heating Fuel:</td>
          <td>
             <asp:DropDownList ID="ddlHeaterFuel" runat="server" OnSelectedIndexChanged="ddlHeaterFuel_OnSelectedIndexChanged" AutoPostBack="true">                       
                          <asp:ListItem Value="Electric">Electric</asp:ListItem>
                          <asp:ListItem Value="Non-electric">Non-electric</asp:ListItem>
             </asp:DropDownList>
          </td>
      </tr>
    <tr>
         <td>
            Kit Selection:
        </td>
        <td>
           <asp:DropDownList ID="ddlKit" runat="server">
                        <asp:ListItem Value="Electric Kit">Electric Kit</asp:ListItem>
                        <asp:ListItem Value="Non-electric Kit">Non-electric Kit</asp:ListItem>                     
           </asp:DropDownList>
        </td>
    </tr>
    <tr>
           <td>
              More Info:
          </td>
          <td>
             <asp:DropDownList ID="ddlMoreInfo" runat="server">
                  <asp:ListItem Value=""></asp:ListItem>
                  <asp:ListItem Value="True">True</asp:ListItem>
                  <asp:ListItem Value="False">False</asp:ListItem>                     
             </asp:DropDownList>
          </td>
      </tr>
    <tr>
         <td>
            Operating Company:
        </td>
        <td>
           <asp:TextBox ID="txtOperatingCompany" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr>
        <td></td>
        <td></td>
    </tr>
    
    <tr runat="server">
        <td colspan="2">            
            <asp:LinkButton id="btnUpdate" text="Save" runat="server" CommandName="Update" Visible='<%# !(DataItem is Telerik.Web.UI.GridInsertionObject) %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton id="btnInsert" text="Save" runat="server" CommandName="PerformInsert" Visible='<%# DataItem is Telerik.Web.UI.GridInsertionObject %>' ValidationGroup="Card" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnCancel" text="Cancel" runat="server" causesvalidation="False" commandname="Cancel" Font-Size="Medium"></asp:LinkButton>
            <asp:LinkButton ID="btnDelete" text="Delete" runat="server" causesvalidation="False" CommandName="Cancel" OnClick="btnDelete_OnClick" Font-Size="Medium"></asp:LinkButton>
         </td>
    </tr>       
</table>