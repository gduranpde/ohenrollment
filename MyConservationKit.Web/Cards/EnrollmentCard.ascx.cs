﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI.WebControls;
using CFLKits.Data.Entities;
using CFLKits.Data.Presenters;
using CFLKits.Entities;
using CyberianSoft.Common;
using Telerik.Web.UI;

namespace CFLKits.Web.Cards
{
    public partial class EnrollmentCard : System.Web.UI.UserControl
    {
        private object _dataItem = null;
        private EnrollmentsPresenter presenter = new EnrollmentsPresenter();

        public Int64 EnrollmentId
        {
            get
            {
                return (!string.IsNullOrEmpty(HiddenField_Id.Value))
                           ? Convert.ToInt64(HiddenField_Id.Value)
                           : 0;
            }
            set { HiddenField_Id.Value = value.ToString(); }
        }

        public object DataItem
        {
            get { return _dataItem; }
            set { _dataItem = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private void Page_PreRender(object sender, EventArgs e)
        {
           
        }

        protected void ddlWaterHeaterFuel_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlWaterHeaterFuel.SelectedValue == "Electric")
                ddlKit.Visible = true;
            else
                ddlKit.Visible = false;
        }

        protected void ddlHeaterFuel_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlHeaterFuel.SelectedValue == "Electric")
                ddlKit.Visible = true;
            else
                ddlKit.Visible = false;
        }

        protected override void OnInit(EventArgs e)
        {
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete?');");

            InitializeComponent();
            base.OnInit(e);
        }

        protected void btnDelete_OnClick(object sender, EventArgs e)
        {
            try
            {
                presenter.Delete(EnrollmentId);
            }
            catch (Exception ex)
            {
                //Log.LogError(ex);

                var l = new Label();
                l.Text = "Unable to delete Enrollment. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = Color.Red;
                //Helper.ErrorInGrid("Enrollment", Page, l);
            }
        }


        private void InitializeComponent()
        {
            DataBinding += new EventHandler(Init_DataBinding);
        }

        private void Init_DataBinding(object sender, EventArgs e)
        {
            InitData();
        }

        public void InitData()
        {
            if ((DataItem != null) & (DataItem.GetType() != typeof(GridInsertionObject)))
            {
                var entity = (Enrollment)DataItem;

                txtInvitationCode.Text = entity.InvitationCode;
                txtAccountNumber.Text = entity.AccountNumber;
                txtAccountNumber12.Text = entity.AccountNumber12;
                txtContactEmail.Text = entity.ContactEmail;
                txtContactFirstName.Text = entity.ContactFirstName;
                txtContactLastName.Text = entity.ContactLastName;
                txtContactPhone.Text = entity.ContactPhone;
                txtContactZipCode.Text = entity.ContactZipCode;

                lstShippingOptions.SelectedValue = entity.ShipToAddress;
                txtAddress1.Text = entity.Address1;
                txtAddress2.Text = entity.Address2;
                txtCity.Text = entity.City;
                txtState.Text = entity.State;
                txtZip.Text = entity.Zip;
                txtOperatingCompany.Text = entity.OperatingCompany;

                RadDatePicker_SubmittedDate.SelectedDate = entity.SubmittedDate;

                if(!string.IsNullOrEmpty(entity.ReferralSource))
                    ddlReferralSources.SelectedValue = entity.ReferralSource;

                if (!string.IsNullOrEmpty(entity.WaterHeaterFuel))
                    ddlWaterHeaterFuel.SelectedValue = entity.WaterHeaterFuel;

                if (!string.IsNullOrEmpty(entity.HeaterFuel))
                    ddlHeaterFuel.SelectedValue = entity.HeaterFuel;

                ddlMoreInfo.SelectedValue = entity.MoreInfo.HasValue ? entity.MoreInfo.Value.ToString() : null;

                if (!string.IsNullOrEmpty(entity.KitSelection) || entity.WaterHeaterFuel == "Electric")
                {
                    ddlKit.SelectedValue = entity.KitSelection;
                    ddlKit.Visible = true;
                }
                else
                    ddlKit.Visible = false;



                EnrollmentId = entity.EnrollmentID;
            }
        }

        public Enrollment FillEntity()
        {
            var entity = new Enrollment();
            entity.EnrollmentID = EnrollmentId;

            entity.AccountNumber = txtAccountNumber.Text;
            entity.AccountNumber12 = txtAccountNumber12.Text;
            entity.InvitationCode = txtInvitationCode.Text;

            entity.ContactEmail = txtContactEmail.Text;
            entity.ContactFirstName = txtContactFirstName.Text;
            entity.ContactLastName = txtContactLastName.Text;
            entity.ContactPhone = txtContactPhone.Text;
            entity.ContactZipCode = txtContactZipCode.Text;

            entity.ShipToAddress = lstShippingOptions.SelectedValue;
            if (entity.ShipToAddress == "O")
            {
                entity.Address1 = txtAddress1.Text;
                entity.Address2 = txtAddress2.Text;
                entity.City = txtCity.Text;
                entity.State = txtState.Text;
                entity.Zip = txtZip.Text;
            }
            else
            {
                entity.Address1 = null;
                entity.Address2 = null;
                entity.City = null;
                entity.State = null;
                entity.Zip = null;
            }

            entity.SubmittedDate = (DateTime)RadDatePicker_SubmittedDate.SelectedDate;
            if (string.IsNullOrEmpty(ddlMoreInfo.SelectedValue))
            {
                entity.MoreInfo = null;
            }
            else
            {
                entity.MoreInfo = bool.Parse(ddlMoreInfo.SelectedValue);
            }
            entity.OperatingCompany = txtOperatingCompany.Text;
            entity.ReferralSource = ddlReferralSources.SelectedValue;
            entity.WaterHeaterFuel = ddlWaterHeaterFuel.SelectedValue;
            entity.HeaterFuel = ddlHeaterFuel.SelectedValue;
            if (entity.WaterHeaterFuel == "Electric" && entity.HeaterFuel == "Electric")
                entity.KitSelection = ddlKit.SelectedValue;
            else
                entity.KitSelection = "Non-electric Kit";

            return entity;
        }



        protected void Page_Init(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
            }
        }
    }
}