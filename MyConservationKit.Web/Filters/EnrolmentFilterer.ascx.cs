﻿using System;
using CFLKits.Data.Filters;

namespace CFLKits.Web.Filters
{
    public partial class EnrolmentFilterer : System.Web.UI.UserControl
    {
        public delegate void EnrolmentFiltererHandler(EnrollmentFilter filter);
        public event EnrolmentFiltererHandler OnFilter;

        public delegate void ConsumerRequestFilterClearHandler(EnrollmentFilter filter);
        public event ConsumerRequestFilterClearHandler OnClear;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button_Filter_Click(object sender, EventArgs e)
        {
            EnrollmentFilter filter = new EnrollmentFilter();
            filter.FromDate = RadDatePicker_StartDate.SelectedDate;
            if (RadDatePicker_EndDate.SelectedDate != null)
                filter.ToDate = RadDatePicker_EndDate.SelectedDate.Value.AddDays(1);
            filter.AccountNumber = txtAccount.Text;
            filter.AccountNumber12 = txtAccount12.Text;
            filter.InvitationCode = txtInvitationCode.Text;
            filter.ContactEmail = txtEmail.Text;
            filter.ContactFirstName = txtFirstName.Text;
            filter.ContactLastName = txtLastName.Text;

            if (string.IsNullOrEmpty(filter.AccountNumber))
                filter.AccountNumber = null;
            if (string.IsNullOrEmpty(filter.AccountNumber12))
                filter.AccountNumber12 = null;
            if (string.IsNullOrEmpty(filter.InvitationCode))
                filter.InvitationCode = null;
            if (string.IsNullOrEmpty(filter.ContactEmail))
                filter.ContactEmail = null;
            if (string.IsNullOrEmpty(filter.ContactFirstName))
                filter.ContactFirstName = null;
            if (string.IsNullOrEmpty(filter.ContactLastName))
                filter.ContactLastName = null;
            
            OnFilter(filter);
        }

        protected void Button_Clear_Click(object sender, EventArgs e)
        {
            RadDatePicker_StartDate.SelectedDate = null;
            RadDatePicker_EndDate.SelectedDate = null;
            txtAccount.Text = null;
            txtAccount12.Text = null;
            txtInvitationCode.Text = null;
            txtEmail.Text = null;
            txtFirstName.Text = null;
            txtLastName.Text = null;

            EnrollmentFilter filter = new EnrollmentFilter();
            OnClear(filter);
        }
    }
}