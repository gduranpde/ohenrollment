﻿using System;
using CFLKits.Data.Presenters;
using CFLKits.Entities;

namespace CFLKits.Web
{
    public partial class ConfirmationPage : System.Web.UI.Page
    {
        EnrollmentsPresenter presenter = new EnrollmentsPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Enrollment"] == null)
                Response.Redirect("EnrollmentRequest.aspx");

            Enrollment enrollment = (Enrollment)Session["Enrollment"];

            //lbAccountNumber.Text = enrollment.AccountNumber + enrollment.AccountNumber12;

            if (Session["AC"] != null && Convert.ToString(Session["AC"]).Trim() != "")
            {
                lbAccountNumber.Text = enrollment.AccountNumber;
            }
            if (Session["NAC"] != null && Convert.ToString(Session["NAC"]).Trim() != "")
            {
                lbAccountNumber.Text = enrollment.AccountNumber12;
            }

            if (Session["ICode"] != null && Convert.ToString(Session["ICode"]).Trim() != "")
            {
                lbInvitationCode.Text = enrollment.InvitationCode;
            }

            //lbInvitationCode.Text = enrollment.InvitationCode;
            lbContactFirstName.Text = enrollment.ContactFirstName;
            lbContactLastName.Text = enrollment.ContactLastName;
            lbContactEmail.Text = enrollment.ContactEmail;
            lbContactPhone.Text = enrollment.FormattedPhone;
            lbContactZipCode.Text = enrollment.ContactZipCode;

            lbReferralSource.Text = enrollment.ReferralSource;
            lbWaterHeaterFuel.Text = enrollment.WaterHeaterFuel;
            lbHeaterFuel.Text = enrollment.HeaterFuel;
            if (string.Equals(enrollment.MoreInfo.ToString(), "true", StringComparison.InvariantCultureIgnoreCase))
            {
                labelMoreInfo.Text = "Yes";
            }
            else 
            {
                labelMoreInfo.Text = "No";
            }
            //labelMoreInfo.Text = (enrollment.MoreInfo.ToString());
            //lbKit.Text = enrollment.KitSelection;

            lbShippingAddressType.Text = enrollment.ShipToAddress;
            switch (enrollment.ShipToAddress)
            {
                case ("O"):
                    lbShippingAddressType.Text = "Other";
                    break;
                case ("M"):
                    lbShippingAddressType.Text = "Mailing Address";
                    break;
                case ("S"):
                    lbShippingAddressType.Text = "Service Address";
                    break;
            }

            if (enrollment.ShipToAddress == "O")
            {
                trAdr1.Visible = true;
                trAdr2.Visible = true;
                trCity.Visible = true;
                trState.Visible = true;
                trZip.Visible = true;
            }
            lbAddressLine1.Text = enrollment.Address1;
            lbAddressLine2.Text = enrollment.Address2;
            lbCity.Text = enrollment.City;
            lbZipOtherShipping.Text = enrollment.Zip;
        }
    }
}