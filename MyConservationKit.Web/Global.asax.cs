﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace MyConservationKit.Web
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!Request.IsSecureConnection)
            {
                string path = string.Format("https{0}", Request.Url.AbsoluteUri.Substring(4));

                Response.Redirect(path);
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception err = (Exception)Server.GetLastError().InnerException;

            if (err != null)
            {
                string strFileName = "Err_dt_" + DateTime.Now.Month + "_" + DateTime.Now.Day
                                        + "_" + DateTime.Now.Year + "_Time_" + DateTime.Now.Hour + "_" +
                                        DateTime.Now.Minute + "_" + DateTime.Now.Second + "_"
                                        + DateTime.Now.Millisecond + ".txt";

                strFileName = Server.MapPath("~") + "\\MyError\\" + strFileName;
                FileStream fsOut = File.Create(strFileName);
                StreamWriter sw = new StreamWriter(fsOut);

                //Log the error details
                string errorText = "Error Message: " + err.Message + sw.NewLine;
                errorText = errorText + "Stack Trace: " + err.StackTrace + sw.NewLine;
                sw.WriteLine(errorText);
                sw.Flush();
                sw.Close();
                fsOut.Close();
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}