﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Shared/Default.Master" AutoEventWireup="true"
    CodeBehind="MasterCustomerDatabaseUpdate.aspx.cs" Inherits="WPP.Web.MasterCustomerDatabaseUpdate" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div class="pageDiv">
        <table>
            <tr>
                <td>
                    <h3>
                        Upload New Master Customer Database</h3>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;Please select a file to upload.
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                The Master Customer Database file must be in CSV format with a<br />
                                header row and 5 columns: Invitation Code, Account Number (old),<br />
                                Account Number (new), Mailing ZIP and Service ZIP.
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label1" runat="server" Text="File Name :"></asp:Label>
                                        </td>
                                        <td style="width: 20px">
                                        </td>
                                        <td>
                                            <telerik:RadUpload ID="RadUpload1" runat="server" ControlObjectsVisibility="None"
                                                Height="24px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="Button1" runat="server" Text="Upload" OnClick="Button1_Click" />
                                                </td>
                                                <td style="width:35px">
                                                </td>
                                                <td>
                                                    <asp:Button ID="Button2" runat="server" Text="Cancel" />
                                                </td>
                                            </tr>
                                         </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="SideContent" runat="server">
</asp:Content>