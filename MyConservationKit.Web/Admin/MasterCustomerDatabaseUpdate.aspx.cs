﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CFLKits.Data.Presenters;
using CFLKits.Entities;
using Telerik.Web.UI;

namespace WPP.Web
{
    public partial class MasterCustomerDatabaseUpdate : System.Web.UI.Page
    {
        EnrollmentsPresenter presenter = new EnrollmentsPresenter();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            UploadedFile file = null;

            foreach (string fileInputID in Request.Files)
            {
                file = UploadedFile.FromHttpPostedFile(Request.Files[fileInputID]);

                if (file.ContentLength > 0 && file.GetExtension() == ".csv")
                {
                    file.SaveAs(Server.MapPath("~/csvUpload/") + file.FileName);

                    string filePath = Server.MapPath("~/csvUpload/") + file.FileName;

                    try
                    {
                        presenter.UploadTable(filePath);
                        int recCount = presenter.GetInvitationRecordCount();
                        Response.Redirect("~/InvitationCodeUpdateStatus.aspx?No=" + recCount);
                    }
                    catch
                    {
                        lblError.Text = "Invalid file format";
                    }
                }
                else
                {
                    lblError.Text = "Please select a CSV file to upload";
                }
            }
        }
    }
}