﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EnrollmentManagement.aspx.cs"
    Inherits="CFLKits.Web.Admin.EnrollmentManagement" MasterPageFile="~/App_Shared/Default.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../Filters/EnrolmentFilterer.ascx" TagName="EnrollmentFilterer"
    TagPrefix="uc1" %>
<asp:Content runat="server" ContentPlaceHolderID="Content">
    <table>
        <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td>
                            <uc1:EnrollmentFilterer ID="EnrollmentFilterer1" runat="server" />
							<asp:Button ID="btnBulkExport" runat="server" OnClick="btnBulkExport_OnClick"
								Text="Bulk Export" />
                        </td>
                        <td style="vertical-align: bottom" align="right">
                            <asp:Button ID="btnUpload" runat="server" Text="Master Customer Database Update"
                                OnClick="btnUpload_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
          <td>
            <label>Records shown in italics have not been replicated to PDE backup servers. Please notify the IT team.</label>
          </td>
        </tr>
        <tr>
            <td>
                <%--<div style="overflow:auto; width:920px">--%>
                <telerik:RadGrid ID="RadGrid_Enrollments" runat="server" Width="920" GridLines="None"
                    Height="600" AutoGenerateColumns="false" PageSize="15" AllowSorting="True" AllowPaging="True"
                    Skin="Windows7" OnNeedDataSource="RadGrid_Enrollments_NeedDataSource" OnItemCommand="RadGrid_Enrollments_ItemCommand"
                    OnItemCreated="RadGrid_Enrollments_ItemCreated" OnUpdateCommand="RadGrid_Enrollments_UpdateCommand"
                    OnItemDataBound="RadGrid_Enrollments_ItemDataBound">
                    <ExportSettings ExportOnlyData="true" IgnorePaging="true">
                    </ExportSettings>
                    <ClientSettings>
                        <Selecting AllowRowSelect="true" />
                        <Resizing AllowColumnResize="True" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
                    </ClientSettings>
                    <MasterTableView DataKeyNames="EnrollmentID" AllowMultiColumnSorting="True" Width="3000"
                        CommandItemDisplay="Top" AutoGenerateColumns="false" EditMode="EditForms" PagerStyle-AlwaysVisible="true"
                        PagerStyle-Mode="NextPrevNumericAndAdvanced">
                       
                       <CommandItemTemplate>
                            <table class="rcCommandTable" width="100%">
                                <td align ="left">
                                    <asp:Button ID="Button1" runat="server" Text=" " CssClass="rgExpXLS" CommandName="ExportToExcel" Title="Export to Excel" style="margin-right:2px; margin-left:2px; margin-top:2px; margin-bottom:2px;"/>
                                    <asp:Button ID="Button2" runat="server" Text=" " CssClass="rgExpPDF" CommandName="ExportToPdf" Title="Export to PDF" style="margin-right:2px; margin-left:2px; margin-top:2px; margin-bottom:2px;"/>
                                    <asp:Button ID="Button3" runat="server" Text=" " CssClass="rgExpCSV" CommandName="ExportToCsv" Title="Export to CSV" style="margin-right:2px; margin-left:2px; margin-top:2px; margin-bottom:2px;"/>
                                    <asp:Button ID="Button4" runat="server" Text=" " CssClass="rgExpDOC" CommandName="ExportToWord" Title="Export to Word" style="margin-right:2px; margin-left:2px; margin-top:2px; margin-bottom:2px;" />
                                </td>
                                <td align="right">         
                                    <asp:Button ID="Button5" runat="server" Text=" " Title="Refresh" CssClass="rgRefresh" CommandName="Refresh" style="margin-right:0px;"/> 
                                    <asp:LinkButton ID="linkButton1" runat="server" Text="Refresh" CommandName="Refresh" style ="vertical-align:top; margin-left:0px;" />
                                </td>
                            </table>
                        </CommandItemTemplate>

                        <Columns>
                            <telerik:GridEditCommandColumn UniqueName="EditCommandColumn">
                                <HeaderStyle Width="50px" />
                                <ItemStyle />
                            </telerik:GridEditCommandColumn>
                            <telerik:GridBoundColumn DataField="EnrollmentID" HeaderText="Enrollment ID" SortExpression="EnrollmentID"
                                UniqueName="EnrollmentID" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false" ReadOnly="true" >
                                 <HeaderStyle Width="100px"/>
                            </telerik:GridBoundColumn>
                            <telerik:GridDateTimeColumn DataField="SubmittedDate" HeaderText="Submitted Date"
                                SortExpression="SubmittedDate" UniqueName="SubmittedDate" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="true" DataFormatString="{0:MM/dd/yyyy}">
                                <HeaderStyle Width="105px"/>
                            </telerik:GridDateTimeColumn>
                          <telerik:GridDateTimeColumn DataField="OperatingCompany" HeaderText="Operating Company"
                                SortExpression="OperatingCompany" UniqueName="OperatingCompany" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">                                
                            </telerik:GridDateTimeColumn>
                            <telerik:GridBoundColumn DataField="InvitationCode" HeaderText="Invitation Code" SortExpression="InvitationCode"
                                UniqueName="InvitationCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false">
                                <HeaderStyle Width="105px"/>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account Number" SortExpression="AccountNumber"
                                UniqueName="AccountNumber" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false">
                                <%-- <HeaderStyle Width="80px"/>--%>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="AccountNumber12" HeaderText="Account Number New"
                                SortExpression="AccountNumber12" UniqueName="AccountNumber12" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                <%-- <HeaderStyle Width="80px"/>--%>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ContactFirstName" HeaderText="Contact FirstName"
                                SortExpression="ContactFirstName" UniqueName="ContactFirstName" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                <%-- <HeaderStyle Width="80px"/>--%>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ContactLastName" HeaderText="ContactLastName"
                                SortExpression="ContactLastName" UniqueName="ContactLastName" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                <%-- <HeaderStyle Width="80px"/>--%>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ContactEmail" HeaderText="ContactEmail" SortExpression="ContactEmail"
                                UniqueName="ContactEmail" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false">
                                <%-- <HeaderStyle Width="80px"/>--%>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ContactPhone" HeaderText="ContactPhone" SortExpression="ContactPhone"
                                UniqueName="ContactPhone" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false">
                                <%-- <HeaderStyle Width="80px"/>--%>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ContactZipCode" HeaderText="ContactZipCode" SortExpression="ContactZipCode"
                                UniqueName="ContactZipCode" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false">
                                 <HeaderStyle Width="100px"/>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ShipToAddress" HeaderText="ShipToAddress" SortExpression="ShipToAddress"
                                UniqueName="ShipToAddress" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false">
                                 <HeaderStyle Width="90px"/>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Address1" HeaderText="Address1" SortExpression="Address1"
                                UniqueName="Address1" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false">
                                <%-- <HeaderStyle Width="80px"/>--%>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Address2" HeaderText="Address2" SortExpression="Address2"
                                UniqueName="Address2" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false">
                                <%-- <HeaderStyle Width="80px"/>--%>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="City" HeaderText="City" SortExpression="City"
                                UniqueName="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false">
                                <%-- <HeaderStyle Width="80px"/>--%>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="State" HeaderText="State" SortExpression="State"
                                UniqueName="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false">
                                 <HeaderStyle Width="80px"/>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Zip" HeaderText="Zip" SortExpression="Zip" UniqueName="Zip"
                                AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                 <HeaderStyle Width="80px"/>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ReferralSource" HeaderText="Referral Source"
                                SortExpression="ReferralSource" UniqueName="ReferralSource" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                <%-- <HeaderStyle Width="80px"/>--%>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="WaterHeaterFuel" HeaderText="Water Heater Fuel"
                                SortExpression="WaterHeaterFuel" UniqueName="WaterHeaterFuel" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                <%-- <HeaderStyle Width="80px"/>--%>
                            </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="HeaterFuel" HeaderText="Heater Fuel"
                                SortExpression="HeaterFuel" UniqueName="HeaterFuel" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                <%-- <HeaderStyle Width="80px"/>--%>
                            </telerik:GridBoundColumn>
                          <telerik:GridBoundColumn DataField="MoreInfo" HeaderText="More Info"
                                SortExpression="MoreInfo" UniqueName="MoreInfo" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false">
                                 <HeaderStyle Width="75px"/>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="KitSelection" HeaderText="Kit" SortExpression="KitSelection"
                                UniqueName="KitSelection" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"
                                ShowFilterIcon="false">
                                <%-- <HeaderStyle Width="80px"/>--%>
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ReplicationStatus" HeaderText="ReplicationStatus"
                                SortExpression="ReplicationStatus" UniqueName="ReplicationStatus" AutoPostBackOnFilter="true"
                                CurrentFilterFunction="Contains" ShowFilterIcon="false" Visible="false">
                                <%-- <HeaderStyle Width="80px"/>--%>
                            </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings UserControlName="../Cards/EnrollmentCard.ascx" EditFormType="WebUserControl">
                            <EditColumn UniqueName="EditCommandColumn1">
                            </EditColumn>
                        </EditFormSettings>
                    </MasterTableView>
                </telerik:RadGrid>
                <%-- <HeaderStyle Width="80px"/>--%>
            </td>
        </tr>
    </table>
    <script type="text/javascript">

        function Expand(itemID) {
            var Grid = $find('<%=RadGrid_Enrollments.ClientID %>');
            var scrollArea = document.getElementById('RadGrid_Enrollments')
            var rowElement = document.getElementById(itemID);
            window.scrollTo(0, rowElement.offsetTop + 200);
        }
    </script>
</asp:Content>