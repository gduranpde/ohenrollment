﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI.WebControls;
using CFLKits.Data.Filters;
using CFLKits.Data.Presenters;
using CFLKits.Entities;
using CFLKits.Web.Cards;
using Telerik.Web.UI;

namespace CFLKits.Web.Admin
{
    public partial class EnrollmentManagement : System.Web.UI.Page
    {
        EnrollmentsPresenter presenter = new EnrollmentsPresenter();
        bool _isExport = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Session["uservalidated"] == null)
                Response.Redirect("Default.aspx");

            if (!Convert.ToBoolean(Session["uservalidated"]))
                Response.Redirect("Default.aspx");
        
            EnrollmentFilterer1.OnFilter += EnrollmentFilterer1_OnFilter;
            EnrollmentFilterer1.OnClear += EnrollmentFilterer1_OnClear;
            
        }

        protected void RadGrid_Enrollments_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (Session["EnrollmentFilter"] != null)
            {
                RadGrid_Enrollments.DataSource = presenter.GetFiltered((EnrollmentFilter)Session["EnrollmentFilter"]);
            }          
            else
            {              
                RadGrid_Enrollments.DataSource = presenter.GetFiltered(new EnrollmentFilter());
        }
        }

        protected void RadGrid_Enrollments_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.EditCommandName)
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem item = (GridDataItem)e.Item;
                    //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "scroll", "setTimeout(function(){ Expand('" + item.ClientID + "');}, 100);", true);
                }
            }

            if (e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToExcelCommandName
                || e.CommandName == RadGrid.ExportToWordCommandName || e.CommandName == RadGrid.ExportToPdfCommandName)
            {
                _isExport = true;
            }
            if (e.CommandName == RadGrid.ExportToCsvCommandName)
            {
                RadGrid_Enrollments_GridExporting();
            }

        }

        void RadGrid_Enrollments_GridExporting()
        {
            RadGrid_Enrollments.MasterTableView.GetColumn("EditCommandColumn").Visible = false;
            RadGrid_Enrollments.MasterTableView.ExportToCSV();
            RadGrid_Enrollments.MasterTableView.Rebind();
        }

       
        protected void RadGrid_Enrollments_ItemCreated(object sender, GridItemEventArgs e)
        {
            try
            {
                if (e.Item is GridCommandItem)
                {
                    Button spriteAddButton = e.Item.FindControl("AddNewRecordButton") as Button;
                    if (spriteAddButton != null)
                    {
                        spriteAddButton.Visible = false;
                    }
                    LinkButton addButton = e.Item.FindControl("InitInsertButton") as LinkButton;
                    if (addButton != null)
                    {
                        addButton.Visible = false;
                    }
                }
            }
            catch { }
        }

        protected void RadGrid_Enrollments_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem) // gets the row collection
            {
                GridDataItem item = (GridDataItem)e.Item;

                string a = item["ReplicationStatus"].Text;

                if (a == "False")
                {
                    item.Font.Italic = true;
                }
            }

            if (e.Item is GridDataItem && _isExport)
            {
                RadGrid_Enrollments.MasterTableView.GetColumn("EditCommandColumn").Display = false;
            }
        }

        protected void RadGrid_Enrollments_UpdateCommand(object source, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == RadGrid.UpdateCommandName)
                {
                    if (e.Item is GridEditFormItem)
                    {
                        GridEditFormItem item = (GridEditFormItem)e.Item;
                        Int64 id = Convert.ToInt64(item.GetDataKeyValue("EnrollmentID").ToString());
                        if (id != 0)
                        {
                            if (e.Item is GridEditableItem)
                            {
                                object o = ((EnrollmentCard)e.Item.FindControl(GridEditFormItem.EditFormUserControlID)).FillEntity();
                                Enrollment enrNew = (Enrollment)o;

                                Enrollment enr = presenter.GetById(enrNew.EnrollmentID);

                                enr.AccountNumber = enrNew.AccountNumber;
                                enr.AccountNumber12 = enrNew.AccountNumber12;
                                enr.InvitationCode = enrNew.InvitationCode;
                                enr.ContactEmail = enrNew.ContactEmail;
                                enr.ContactFirstName = enrNew.ContactFirstName;
                                enr.ContactLastName = enrNew.ContactLastName;
                                enr.ContactPhone = enrNew.ContactPhone;
                                enr.ContactZipCode = enrNew.ContactZipCode;
                                enr.SubmittedDate = enrNew.SubmittedDate;

                                enr.ShipToAddress = enrNew.ShipToAddress;
                                enr.Address1 = enrNew.Address1;
                                enr.Address2 = enrNew.Address2;
                                enr.City = enrNew.City;
                                enr.State = enrNew.State;
                                enr.Zip = enrNew.Zip;
                                enr.ReferralSource = enrNew.ReferralSource;                                
                                enr.WaterHeaterFuel = enrNew.WaterHeaterFuel;
                                enr.HeaterFuel = enrNew.HeaterFuel;
                                enr.MoreInfo = enrNew.MoreInfo;
                                enr.KitSelection = enrNew.KitSelection;
                                enr.OperatingCompany = enrNew.OperatingCompany;

                                presenter.SaveEnrollment(enr, true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.LogError(ex);

                Label l = new Label();
                l.Text = "Unable to update Enrollment. Reason: " + ex.Message;
                l.ControlStyle.ForeColor = System.Drawing.Color.Red;
                RadGrid_Enrollments.Controls.Add(l);
            }
        }

        private void EnrollmentFilterer1_OnFilter(EnrollmentFilter filter)
        {
            Session["EnrollmentFilter"] = filter;
            RadGrid_Enrollments.Rebind();
        }

        private void EnrollmentFilterer1_OnClear(EnrollmentFilter filter)
        {
            Session["EnrollmentFilter"] = filter;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            //Response.Redirect("~/MasterCustomerDatabaseUpdate.aspx");
            Response.Redirect("MasterDatabaseUpdateHistoryPage.aspx");
        }


		protected void btnBulkExport_OnClick(object sender, EventArgs e)
		{
			var source = Session["EnrollmentFilter"] != null ? 
							presenter.GetFiltered((EnrollmentFilter) Session["EnrollmentFilter"]) : 
							presenter.GetFiltered(new EnrollmentFilter());

			if (source.Count > 0)
			{
				ProduceCsv(source);
			}
		}

		private void ProduceCsv(IEnumerable<Enrollment> source)
		{
			var values = new string[22];

			#region Make the Header
			values[0] = "EnrollmentID";
			values[1] = "SubmittedDate";
			values[2] = "OperatingCompany";
			values[3] = "InvitationCode";
			values[4] = "AccountNumber";
			values[5] = "AccountNumber12";
			values[6] = "ContactFirstName";
			values[7] = "ContactLastName";
			values[8] = "ContactEmail";
			values[9] = "ContactPhone";
			values[10] = "ContactZipCode";
			values[11] = "ShipToAddress";
			values[12] = "Address1";
			values[13] = "Address2";
			values[14] = "City";
			values[15] = "State";
			values[16] = "Zip";
			values[17] = "ReferralSource";
			values[18] = "WaterHeaterFuel";
			values[19] = "HeaterFuel";
			values[20] = "MoreInfo";
			values[21] = "KitSelection";
			#endregion

			Response.ClearContent();
			Response.Clear();
			Response.ContentType = "application/CSV";
			Response.AddHeader("Content-Disposition", "attachment;filename=EnrollmentManagement.csv");
			Response.Write(string.Join(",", values));
			Response.Write(Environment.NewLine);

			if (source != null)
			{
				foreach (var item in source)
				{
					values = new string[22];
					foreach (var prop in item.GetType().GetProperties())
					{
						var propValue = prop.GetValue(item, null);

						#region Make items
						switch (prop.Name)
						{
							case "EnrollmentID":
								values[0] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "SubmittedDate":
								values[1] = propValue != null ? "\"" + Convert.ToDateTime(propValue).ToString("MM/dd/yyyy") + "\"" : string.Empty;
								break;
							case "OperatingCompany":
								values[2] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "InvitationCode":
								values[3] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "AccountNumber":
								values[4] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "AccountNumber12":
								values[5] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "ContactFirstName":
								values[6] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "ContactLastName":
								values[7] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "ContactEmail":
								values[8] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "ContactPhone":
								values[9] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "ContactZipCode":
								values[10] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "ShipToAddress":
								values[11] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "Address1":
								values[12] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "Address2":
								values[13] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "City":
								values[14] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "State":
								values[15] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "Zip":
								values[16] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "ReferralSource":
								values[17] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "WaterHeaterFuel":
								values[18] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "HeaterFuel":
								values[19] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "MoreInfo":
								values[20] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
							case "KitSelection":
								values[21] = propValue != null ? "\"" + propValue + "\"" : string.Empty;
								break;
						}
						#endregion
					}
					Response.Write(string.Join(",", values));
					Response.Write(Environment.NewLine);
				}
			}

			Response.Flush();
			Response.End();
		}
    }
}